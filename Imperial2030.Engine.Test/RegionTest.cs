﻿using System.Collections.Immutable;
using System.Linq;
using NUnit.Framework;

namespace Imperial2030.Engine.Test
{
    public class RegionTest
    {
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");

        private static readonly ImmutableDictionary<Identity, MilitaryCollection> NoMilitary = ImmutableDictionary<Identity, MilitaryCollection>.Empty;

        [Test]
        public void RegionWithoutMilitaryCanNotHaveBattle()
        {
            var region = NewRegion(NoMilitary);
            
            Assert.That(region.CanHaveBattle, Is.False);
        }

        [Test]
        public void RegionWithSingleMilitaryCanNotHaveBattle()
        {
            var region = NewRegion(RegionMilitary(NationIdentity, Military.Army));
            
            Assert.That(region.CanHaveBattle, Is.False);
        }

        [Test]
        public void RegionWithMultipleMilitaryFromSingleNationCanNotHaveBattle()
        {
            var region = NewRegion(RegionMilitary(NationIdentity, Military.Army, Military.Army, Military.Fleet));
            
            Assert.That(region.CanHaveBattle, Is.False);
        }

        [Test]
        public void RegionWithMilitaryFromMultipleNationsCanHaveBattle()
        {
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, Military.Army),
                    (OpponentIdentity, Military.Army)));
            
            Assert.That(region.CanHaveBattle);
        }

        [Test]
        public void BattleDestroysSelectedMilitaryFromBothNations()
        {
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, new MilitaryCollection(Military.Army, Military.Fleet)),
                    (OpponentIdentity, new MilitaryCollection(Military.Army, Military.Fleet))));
            Assume.That(region.CanHaveBattle);
            Assume.That(region.ControllingNation, Is.Null);

            var newRegion = region.Battle(NationIdentity, Military.Army, OpponentIdentity, Military.Fleet)
                .GetValueOrThrow("test setup failed (unable to init battle)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity).Contains(Military.Fleet));
            Assert.That(newRegion.MilitaryOf(OpponentIdentity).Contains(Military.Army));
            Assert.That(newRegion.ControllingNation, Is.Null);
        }

        [Test]
        public void AfterBattleWhenOnlyActivatorHasRemainingMilitaryItBecomesControllingNation()
        {
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, new MilitaryCollection(Military.Army, Military.Fleet)),
                    (OpponentIdentity, new MilitaryCollection(Military.Army))));
            Assume.That(region.CanHaveBattle);
            Assume.That(region.ControllingNation, Is.Null);

            var newRegion = region.Battle(NationIdentity, Military.Army, OpponentIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to init battle)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Not.Empty);
            Assert.That(newRegion.MilitaryOf(OpponentIdentity), Is.Empty);
            Assert.That(newRegion.ControllingNation, Is.EqualTo(NationIdentity));
        }

        [Test]
        public void AfterBattleWhenOnlyTargetHasRemainingMilitaryItBecomesControllingNation()
        {
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, new MilitaryCollection(Military.Army)),
                    (OpponentIdentity, new MilitaryCollection(Military.Army, Military.Fleet))));
            Assume.That(region.CanHaveBattle);
            Assume.That(region.ControllingNation, Is.Null);

            var newRegion = region.Battle(NationIdentity, Military.Army, OpponentIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to init battle)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Empty);
            Assert.That(newRegion.MilitaryOf(OpponentIdentity), Is.Not.Empty);
            Assert.That(newRegion.ControllingNation, Is.EqualTo(OpponentIdentity));
        }

        [Test]
        public void AfterBattleWhenNoOneIsLeftControllingNationDoesNotChange()
        {
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, new MilitaryCollection(Military.Army)),
                    (OpponentIdentity, new MilitaryCollection(Military.Army))),
                OpponentIdentity);
            Assume.That(region.CanHaveBattle);
            Assume.That(region.ControllingNation, Is.EqualTo(OpponentIdentity));

            var newRegion = region.Battle(NationIdentity, Military.Army, OpponentIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to init battle)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Empty);
            Assert.That(newRegion.MilitaryOf(OpponentIdentity), Is.Empty);
            Assert.That(newRegion.ControllingNation, Is.EqualTo(OpponentIdentity));
        }

        [Test]
        public void AfterBattleLastRemainingNeutralNationBecomesControllingNation()
        {
            var neutralIdentity = new Identity("neutral");
            var region = NewRegion(
                RegionMilitary(
                    (NationIdentity, new MilitaryCollection(Military.Army)),
                    (OpponentIdentity, new MilitaryCollection(Military.Army)),
                    (neutralIdentity, new MilitaryCollection(Military.Army))));
            Assume.That(region.CanHaveBattle);
            Assume.That(region.ControllingNation, Is.Null);

            var newRegion = region.Battle(NationIdentity, Military.Army, OpponentIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to init battle)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Empty);
            Assert.That(newRegion.MilitaryOf(OpponentIdentity), Is.Empty);
            Assert.That(newRegion.MilitaryOf(neutralIdentity), Is.Not.Empty);
            Assert.That(newRegion.ControllingNation, Is.EqualTo(neutralIdentity));
        }

        [Test]
        public void MoveLastMilitaryOutOfRegion()
        {
            var region = NewRegion(RegionMilitary(NationIdentity, Military.Army));
            Assume.That(region.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(region.ControllingNation, Is.EqualTo(NationIdentity));

            var newRegion = region.MoveMilitaryOut(NationIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to move military)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Empty);
            Assert.That(newRegion.ControllingNation, Is.EqualTo(NationIdentity));
        }

        [Test]
        public void MoveNotLastMilitaryOutOfRegion()
        {
            var region = NewRegion(RegionMilitary(NationIdentity, Military.Army, Military.Army));
            Assume.That(region.MilitaryOf(NationIdentity).Count, Is.EqualTo(2));
            Assume.That(region.ControllingNation, Is.EqualTo(NationIdentity));

            var newRegion = region.MoveMilitaryOut(NationIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to move military)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assert.That(newRegion.ControllingNation, Is.EqualTo(NationIdentity));
        }

        [Test]
        public void MoveLastMilitaryOutOfRegionWithOpponent()
        {
            var region = NewRegion(RegionMilitary((NationIdentity, Military.Army), (OpponentIdentity, Military.Army)));
            Assert.That(region.ControllingNation, Is.Null);
            
            Assume.That(region.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(region.MilitaryOf(OpponentIdentity).Count, Is.EqualTo(1));

            var newRegion = region.MoveMilitaryOut(NationIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to move military)");
            
            Assert.That(newRegion.MilitaryOf(NationIdentity), Is.Empty);
            Assert.That(newRegion.MilitaryOf(OpponentIdentity).Count, Is.EqualTo(1));
            Assert.That(newRegion.ControllingNation, Is.EqualTo(OpponentIdentity));
        }

        [Test]
        public void MoveFirstMilitaryIntoUnoccupiedRegion()
        {
            var region = NewRegion(NoMilitary);
            Assume.That(region.MilitaryOf(NationIdentity), Is.Empty);
            Assume.That(region.ControllingNation, Is.Null);

            var newRegion = region.MoveMilitaryIn(NationIdentity, Military.Army);
            
            Assert.That(newRegion.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assert.That(newRegion.ControllingNation, Is.EqualTo(NationIdentity));
        }

        [Test]
        public void MoveSecondMilitaryIntoRegionWithoutOpponent()
        {
            var region = NewRegion(RegionMilitary(NationIdentity, Military.Army));
            Assume.That(region.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(region.ControllingNation, Is.EqualTo(NationIdentity));

            var newRegion = region.MoveMilitaryIn(NationIdentity, Military.Army);

            Assert.That(newRegion.MilitaryOf(NationIdentity).Count, Is.EqualTo(2));
            Assert.That(newRegion.ControllingNation, Is.EqualTo(NationIdentity));
        }

        [Test]
        public void MoveMilitaryToRegionWithOpponent()
        {
            var region = NewRegion(RegionMilitary(OpponentIdentity, Military.Army));
            Assume.That(region.MilitaryOf(OpponentIdentity).Count, Is.EqualTo(1));
            Assume.That(region.ControllingNation, Is.EqualTo(OpponentIdentity));

            var newRegion = region.MoveMilitaryIn(NationIdentity, Military.Army);

            Assert.That(newRegion.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assert.That(newRegion.MilitaryOf(OpponentIdentity).Count, Is.EqualTo(1));
            Assert.That(newRegion.ControllingNation, Is.EqualTo(OpponentIdentity));
        }
        
        private static Region NewRegion(ImmutableDictionary<Identity, MilitaryCollection> militaryPerPlayer, Identity controllingNation = null)
        {
            if (controllingNation == null && militaryPerPlayer.Count == 1)
                controllingNation = militaryPerPlayer.Keys.Single();

            var regionIdentity = new Identity("Canada");
            return new LandRegion(regionIdentity, militaryPerPlayer, controllingNation);
        }

        private static ImmutableDictionary<Identity, MilitaryCollection> RegionMilitary(Identity nation, params Military[] military)
        {
            return ImmutableDictionary<Identity, MilitaryCollection>.Empty.Add(nation, new MilitaryCollection(military));
        }

        private static ImmutableDictionary<Identity, MilitaryCollection> RegionMilitary(
            params (Identity nation, MilitaryCollection military)[] militaryPerPlayer)
        {
            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();

            foreach (var (nation, military) in militaryPerPlayer)
            {
                builder.Add(nation, military);
            }

            return builder.ToImmutable();
        }
    }
}
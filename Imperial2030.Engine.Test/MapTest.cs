using NUnit.Framework;

namespace Imperial2030.Engine.Test
{
    public class MapTest
    {
        private static readonly Identity Nl = new Identity("Netherlands");
        private static readonly Identity Be = new Identity("Belgium");
        private static readonly Identity Fr = new Identity("France");
        
        [Test]
        public void AreConnectedInGivenOrder()
        {
            var map = new Map((Nl, Be));
            
            Assert.That(map.AreConnected(Nl, Be));
        }

        [Test]
        public void AreConnectedInReverseOrder()
        {
            var map = new Map((Nl, Be));
            
            Assert.That(map.AreConnected(Be, Nl));
        }

        [Test]
        public void ConnectionOfConnectionAreNotNecessarilyConnected()
        {
            var map = new Map((Nl, Be), (Be, Fr));
            
            Assert.That(map.AreConnected(Nl, Fr), Is.False);
        }

        [Test]
        public void GetConnectionsReturnsGivenAndReverseConnections()
        {
            var map = new Map((Nl, Be), (Be, Fr));

            Assert.That(map.GetConnections(Be), Is.EquivalentTo(new[] {Nl, Fr}));
        }
    }
}
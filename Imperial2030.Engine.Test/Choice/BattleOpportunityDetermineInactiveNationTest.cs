﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Maneuver;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class BattleOpportunityDetermineInactiveNationTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        private static readonly Identity Opponent2Identity = new Identity("Brazil");

        [Test]
        public void HasOneSelectNationOptionPerOpponent()
        {
            var opponents = ImmutableList.Create(OpponentIdentity, Opponent2Identity);
            var region = NewRegion(opponents);
            var battleOpportunity = new BattleOpportunityDetermineInactiveNation(NewManeuver(region), region, Military.Army, opponents, CancelUndo.None);

            var selectNationOptions = battleOpportunity.NationOptions.ToList();
            Assert.That(selectNationOptions, Has.Count.EqualTo(opponents.Count));

            var selectableNations = selectNationOptions.Select(option => option.Nation).ToList();
            Assert.That(selectableNations, Is.EqualTo(opponents));
        }

        [Test]
        public void AllOptionsResultInBattleOpportunityInactiveNation()
        {
            var opponents = ImmutableList.Create(OpponentIdentity, Opponent2Identity);
            var region = NewRegion(opponents);
            var battleOpportunity = new BattleOpportunityDetermineInactiveNation(NewManeuver(region), region, Military.Army, opponents, CancelUndo.None);

            var nextChoices = battleOpportunity.NationOptions
                .Select(option => option.NextChoice)
                .ToList();
            
            Assert.That(nextChoices, Is.All.TypeOf<BattleOpportunityInactiveNation>());
        }

        private static Maneuver NewManeuver(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }

        private static Region NewRegion(ImmutableList<Identity> opponents)
        {
            var regionIdentity = new Identity("Canada");

            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();
            builder.Add(NationIdentity, Military.Army);
            foreach (var nation in opponents)
            {
                builder.Add(nation, Military.Army);
            }
            var militaryPerNation = builder.ToImmutable();
            
            return new LandRegion(regionIdentity, militaryPerNation, null);
        }
    }
}
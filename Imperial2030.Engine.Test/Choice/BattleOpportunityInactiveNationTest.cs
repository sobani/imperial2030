﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Maneuver;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class BattleOpportunityInactiveNationTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        private static readonly Identity RegionIdentity = new Identity("Canada");
        
        private static readonly ImmutableList<Identity> NoOtherInactiveNations = ImmutableList<Identity>.Empty;

        [Test]
        public void HasOneBattleOptionWhenHasOnlyOneMilitaryTypeInRegion()
        {
            var militaryPerNation = ImmutableDictionary<Identity, MilitaryCollection>.Empty
                .Add(NationIdentity, Military.Army)
                .Add(OpponentIdentity, Military.Army);
            var region = new LandRegion(RegionIdentity, militaryPerNation, null);
            
            var maneuver = NewManeuver(region);
            var battleOpportunity = new BattleOpportunityInactiveNation(maneuver, region, Military.Army, OpponentIdentity, NoOtherInactiveNations, CancelUndo.None);

            var battleOptions = battleOpportunity.Options.OfType<BattleOption>().ToList();
            Assert.That(battleOptions, Has.Count.EqualTo(1));
            Assert.That(battleOptions[0].Nation, Is.EqualTo(maneuver.ActiveNationIdentity));
        }

        [Test]
        public void HasTwoBattleOptionsWhenHasBothMilitaryTypesInRegion()
        {
            var militaryPerNation = ImmutableDictionary<Identity, MilitaryCollection>.Empty
                .Add(NationIdentity, Military.Army)
                .Add(OpponentIdentity, new MilitaryCollection(Military.Army, Military.Fleet));
            var region = new LandRegion(RegionIdentity, militaryPerNation, null);
            var maneuver = NewManeuver(region);
            var battleOpportunity = new BattleOpportunityInactiveNation(maneuver, region, Military.Army, OpponentIdentity, NoOtherInactiveNations, CancelUndo.None);

            var battleOptions = battleOpportunity.Options.OfType<BattleOption>().ToList();
            Assert.That(battleOptions, Has.Count.EqualTo(2));
            Assert.That(battleOptions[0].Nation, Is.EqualTo(maneuver.ActiveNationIdentity));
            Assert.That(battleOptions[1].Nation, Is.EqualTo(maneuver.ActiveNationIdentity));
        }

        [Test]
        public void HasOneNoBattleOption()
        {
            var region = NewRegion(NoOtherInactiveNations);
            var battleOpportunity = new BattleOpportunityInactiveNation(NewManeuver(region), region, Military.Army, OpponentIdentity, NoOtherInactiveNations, CancelUndo.None);

            var noBattleOptions = battleOpportunity.Options.OfType<NoBattleOption>().ToList();
            Assert.That(noBattleOptions, Has.Count.EqualTo(1));
        }

        [Test]
        public void NoBattleWithSingleRemainingInactiveNationResultsInBattleOpportunityInactiveNation()
        {
            var remainingInactiveNations = ImmutableList.Create(new Identity("Brazil"));
            var region = NewRegion(remainingInactiveNations);
            var battleOpportunity = new BattleOpportunityInactiveNation(NewManeuver(region), region, Military.Army, OpponentIdentity, remainingInactiveNations, CancelUndo.None);

            var noBattleOptions = battleOpportunity.Options.OfType<NoBattleOption>().ToList();
            Assume.That(noBattleOptions, Has.Count.EqualTo(1));
            Assert.That(noBattleOptions[0].NextChoice, Is.TypeOf<BattleOpportunityInactiveNation>());
        }

        [Test]
        public void NoBattleWithMultipleRemainingInactiveNationResultsInBattleOpportunityDetermineInactiveNation()
        {
            var remainingInactiveNations = ImmutableList.Create(new Identity("Brazil"), new Identity("Russia"));
            var region = NewRegion(remainingInactiveNations);
            var battleOpportunity = new BattleOpportunityInactiveNation(NewManeuver(region), region, Military.Army, OpponentIdentity, remainingInactiveNations, CancelUndo.None);

            var noBattleOptions = battleOpportunity.Options.OfType<NoBattleOption>().ToList();
            Assume.That(noBattleOptions, Has.Count.EqualTo(1));
            Assert.That(noBattleOptions[0].NextChoice, Is.TypeOf<BattleOpportunityDetermineInactiveNation>());
        }

        private static Maneuver NewManeuver(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }

        private static Region NewRegion(ImmutableList<Identity> otherOpponents)
        {
            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();
            builder.Add(NationIdentity, Military.Army);
            builder.Add(OpponentIdentity, Military.Army);
            foreach (var nation in otherOpponents)
            {
                builder.Add(nation, Military.Army);
            }
            var militaryPerNation = builder.ToImmutable();
            
            return new LandRegion(RegionIdentity, militaryPerNation, null);
        }
    }
}
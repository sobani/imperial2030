﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Game;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class FactoryTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        
        private static readonly Identity RegionIdentity = new Identity("Central");

        [Test]
        public void CanBuildFactoryInArmyProvinceWithoutFactory()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, false);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            Assume.That(region.HasFactory, Is.False);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Not.Null);
            var newRegion = selectedOption.NextChoice.Game.World.Regions[region.Identity];
            
            Assert.That(newRegion, Is.TypeOf<ArmyProvince>());
            Assert.That(((ArmyProvince) newRegion).HasFactory);
        }

        [Test]
        public void CanNotBuildFactoryInArmyProvinceWithFactory()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, true);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            Assume.That(region.HasFactory);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Null);
        }

        [Test]
        public void CanBuildFactoryInFleetProvinceWithoutFactory()
        {
            var region = new FleetProvince(RegionIdentity, NationIdentity, false);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            Assume.That(region.HasFactory, Is.False);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Not.Null);
            var newRegion = selectedOption.NextChoice.Game.World.Regions[region.Identity];
            
            Assert.That(newRegion, Is.TypeOf<FleetProvince>());
            Assert.That(((FleetProvince) newRegion).HasFactory);
        }

        [Test]
        public void CanNotBuildFactoryInFleetProvinceWithFactory()
        {
            var region = new FleetProvince(RegionIdentity, NationIdentity, true);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            Assume.That(region.HasFactory);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Null);
        }

        [Test]
        public void CanNotBuildFactoryInLandRegion()
        {
            var region = new LandRegion(RegionIdentity, ImmutableDictionary<Identity, MilitaryCollection>.Empty, NationIdentity);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);

            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Null);
        }

        [Test]
        public void CanNotBuildFactoryInSeaRegion()
        {
            var region = new SeaRegion(RegionIdentity, ImmutableDictionary<Identity, MilitaryCollection>.Empty, NationIdentity);
            var factory = new Factory(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Null);
        }

        [Test]
        public void CanNotBuildFactoryWhenNationDoesNotHaveEnoughMoney()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, false);
            var game = NewGame(region)
                .WithNationMoney(NationIdentity, 4)
                .GetValueOrThrow("test setup failed");
            var factory = new Factory(game, NationIdentity, PlayerIdentity, CancelUndo.None);
            Assume.That(region.HasFactory, Is.False);
            
            var selectedOption = factory.Options
                .OfType<SelectRegionOption>()
                .SingleOrDefault(option => option.Region == RegionIdentity);
            Assert.That(selectedOption, Is.Null);
        }

        private static Game NewGame(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);

            return game.WithNationMoney(NationIdentity, 10)
                .GetValueOrThrow("test setup failed");
        }
    }
}
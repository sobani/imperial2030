﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Maneuver;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class ActivateMilitaryTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        private static readonly Identity Opponent2Identity = new Identity("Brazil");
        private static readonly Identity RegionIdentity = new Identity("Canada");

        [Test]
        public void SingleOpponentHasOneBattleOption()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity);
            var activate = new ActivateMilitary(NewManeuver(region), region, Military.Army);

            var battleOptions = activate.Options.OfType<BattleOption>().ToList();
            Assert.That(battleOptions, Has.Count.EqualTo(1));
            Assert.That(battleOptions[0].Nation, Is.EqualTo(OpponentIdentity));
        }

        [Test]
        public void HasOneBattleOptionPerOpponentInRegion()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity, Opponent2Identity);
            var activate = new ActivateMilitary(NewManeuver(region), region, Military.Army);

            var battleCount = activate.Options.OfType<BattleOption>().Count();
            var opponentCount = region.NationsWithMilitary.Count() - 1;
            Assert.That(battleCount, Is.EqualTo(opponentCount));
        }

        [Test]
        public void HasOneBattleOptionPerOpponentMilitaryTypeInRegion()
        {
            var militaryPerNation = ImmutableDictionary<Identity, MilitaryCollection>.Empty
                .Add(NationIdentity, Military.Army)
                .Add(OpponentIdentity, new MilitaryCollection(Military.Army, Military.Fleet));
            var region = new LandRegion(RegionIdentity, militaryPerNation, null);
            
            var activate = new ActivateMilitary(NewManeuver(region), region, Military.Army);

            var battleCount = activate.Options.OfType<BattleOption>().Count();
            Assert.That(battleCount, Is.EqualTo(2));
        }

        [Test]
        public void BattleRemovesMilitaryOfActiveNationAndOfChosenOpponent()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity, Opponent2Identity);
            var activate = new ActivateMilitary(NewManeuver(region), region, Military.Army);

            var battle = activate.Options.OfType<BattleOption>().SingleOrDefault(option => option.Nation == Opponent2Identity);
            Assert.That(battle, Is.Not.Null);

            var newRegion = battle.NextChoice.Game.World.Regions[region.Identity];

            Assert.That(newRegion.NationsWithMilitary, Is.EqualTo(new[] {OpponentIdentity}));
        }

        [Test]
        public void CancelResultsInPreviousChoice()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity, Opponent2Identity);
            var maneuver = NewManeuver(region);
            var activate = new ActivateMilitary(maneuver, region, Military.Army);

            var cancel = activate.Options.OfType<CancelOption>().SingleOrDefault();
            Assert.That(cancel, Is.Not.Null);

            Assert.That(cancel.PreviousChoice, Is.SameAs(maneuver));
        }

        private static Maneuver NewManeuver(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }

        private static Region NewRegion(params Identity[] nationIdentities)
        {
            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();
            foreach (var nation in nationIdentities)
            {
                builder.Add(nation, Military.Army);
            }
            var militaryPerNation = builder.ToImmutable();
            
            var controllingNation = nationIdentities.Length == 1 ? nationIdentities[0] : null;

            return new LandRegion(RegionIdentity, militaryPerNation, controllingNation);
        }
    }
}
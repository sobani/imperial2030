﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Game;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class ImportTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        
        private static readonly Identity RegionIdentity = new Identity("Central");

        [Test]
        public void CanImportArmyIntoArmyProvince()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, false);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.EquivalentTo(new[] {Military.Army}));
        }

        [Test]
        public void CanImportArmyOrFleetIntoFleetProvince()
        {
            var region = new FleetProvince(RegionIdentity, NationIdentity, false);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.EquivalentTo(new[] {Military.Army, Military.Fleet}));
        }

        [Test]
        public void CanNotImportIntoArmyProvinceOfOpponent()
        {
            var region = new ArmyProvince(RegionIdentity, OpponentIdentity, false);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.Empty);
        }

        [Test]
        public void CanNotImportIntoFleetProvinceOfOpponent()
        {
            var region = new FleetProvince(RegionIdentity, OpponentIdentity, false);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.Empty);
        }

        [Test]
        public void CanNotImportIntoLandRegion()
        {
            var region = new LandRegion(RegionIdentity);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.Empty);
        }

        [Test]
        public void CanNotImportIntoSeaRegion()
        {
            var region = new SeaRegion(RegionIdentity);
            var import = new Import(NewGame(region), NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var militaryOptions = import.Options
                .OfType<ImportOption>()
                .Where(option => option.Region == RegionIdentity)
                .Select(option => option.Military);
            
            Assert.That(militaryOptions, Is.Empty);
        }

        private static Game NewGame(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            
            return game.WithNationMoney(NationIdentity, 10)
                .GetValueOrThrow("test setup failed");
        }
    }
}
﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Maneuver;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class BattleOpportunityActiveNationTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        private static readonly Identity Opponent2Identity = new Identity("Brazil");
        private static readonly Identity RegionIdentity = new Identity("Canada");
        private static readonly Identity RegionFromIdentity = new Identity("regionFrom");
        private static readonly Identity RegionToIdentity = new Identity("regionTo");

        [Test]
        public void SingleOpponentHasOneBattleOption()
        {
            var battleOpportunity = NewBattleOpportunityActiveNation((OpponentIdentity, Military.Army));

            var battleOptions = battleOpportunity.Options.OfType<BattleOption>().ToList();
            Assert.That(battleOptions, Has.Count.EqualTo(1));
            Assert.That(battleOptions[0].Nation, Is.EqualTo(OpponentIdentity));
        }

        [Test]
        public void HasOneBattleOptionPerOpponentInRegion()
        {
            var battleOpportunity = NewBattleOpportunityActiveNation(
                (OpponentIdentity, Military.Army),
                (Opponent2Identity, Military.Army));

            var battleCount = battleOpportunity.Options.OfType<BattleOption>().Count();
            Assert.That(battleCount, Is.EqualTo(2));
        }

        [Test]
        public void HasOneBattleOptionPerOpponentMilitaryTypeInRegion()
        {
            var regionMilitary = new Dictionary<Identity, MilitaryCollection>(){ { OpponentIdentity, new MilitaryCollection(Military.Fleet, Military.Army)}};
            var regionTo = new FleetProvince(RegionToIdentity, OpponentIdentity,
                    regionMilitary.ToImmutableDictionary(), 
                    true);
            Assume.That(
                regionTo.MilitaryOf(OpponentIdentity).Types,
                Is.EquivalentTo(new[] {Military.Army, Military.Fleet}));
            
            var battleOpportunity = NewBattleOpportunityActiveNation(regionTo);

            var battleCount = battleOpportunity.Options.OfType<BattleOption>().Count();
            Assert.That(battleCount, Is.EqualTo(2));
        }

        [Test]
        public void BattleRemovesMilitaryOfActiveNationAndOfChosenOpponent()
        {
            var battleOpportunity = NewBattleOpportunityActiveNation(
                (OpponentIdentity, Military.Army),
                (Opponent2Identity, Military.Army));

            var battle = battleOpportunity.Options.OfType<BattleOption>().SingleOrDefault(option => option.Nation == Opponent2Identity);
            Assert.That(battle, Is.Not.Null);

            var newRegionTo = battle.NextChoice.Game.World.Regions[RegionToIdentity];

            Assert.That(newRegionTo.NationsWithMilitary, Is.EqualTo(new[] {OpponentIdentity}));
        }

        [Test]
        public void NoBattleDoesNotChangeWorld()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity, Opponent2Identity);
            var maneuver = NewManeuver(region);
            var oldWorld = maneuver.Game.World;
            var battleOpportunity = new BattleOpportunityActiveNation(maneuver, region, Military.Army, CancelUndo.None);

            var noBattle = battleOpportunity.Options.OfType<NoBattleOption>().SingleOrDefault();
            Assert.That(noBattle, Is.Not.Null);

            var newWorld = noBattle.NextChoice.Game.World;

            Assert.That(newWorld, Is.SameAs(oldWorld));
        }

        [Test]
        public void NoBattleWithSingleOpponentResultsInBattleOpportunityInactiveNation()
        {
            var battleOpportunity = NewBattleOpportunityActiveNation((OpponentIdentity, Military.Army));

            var noBattle = battleOpportunity.Options.OfType<NoBattleOption>().SingleOrDefault();
            Assume.That(noBattle, Is.Not.Null);
            Assert.That(noBattle.NextChoice, Is.TypeOf<BattleOpportunityInactiveNation>());
        }

        [Test]
        public void NoBattleWithMultipleOpponentResultsInBattleOpportunityDetermineInactiveNationWithAllOpponents()
        {
            var region = NewRegion(NationIdentity, OpponentIdentity, Opponent2Identity);
            var battleOpportunity = new BattleOpportunityActiveNation(NewManeuver(region), region, Military.Army, CancelUndo.None);

            var noBattle = battleOpportunity.Options.OfType<NoBattleOption>().SingleOrDefault();
            Assume.That(noBattle, Is.Not.Null);
            Assert.That(noBattle.NextChoice, Is.TypeOf<BattleOpportunityDetermineInactiveNation>());
            var bodin = (BattleOpportunityDetermineInactiveNation) noBattle.NextChoice;
            Assert.That(bodin.InactiveNationIdentities, Is.EquivalentTo(new[] {OpponentIdentity, Opponent2Identity}));
        }

        private static BattleOpportunityActiveNation NewBattleOpportunityActiveNation(
            params (Identity, Military)[] militaryInDestination)
        {
            var fromRegion = new LandRegion(RegionFromIdentity)
                .MoveMilitaryIn(NationIdentity, Military.Army);
            
            Region toRegion = new LandRegion(RegionToIdentity);
            foreach (var (nation, military) in militaryInDestination)
                toRegion = toRegion.MoveMilitaryIn(nation, military);

            var maneuver = NewManeuver(fromRegion, toRegion);
            var regionManeuver = (RegionManeuver) maneuver.Options.First().NextChoice;
            var battleOpportunity = (BattleOpportunityActiveNation) regionManeuver.Options.First().NextChoice;
            return battleOpportunity;
        }

        private static BattleOpportunityActiveNation NewBattleOpportunityActiveNation(
            Region toRegion)
        {
            var fromRegion = new LandRegion(RegionFromIdentity)
                .MoveMilitaryIn(NationIdentity, Military.Army);
            
            var maneuver = NewManeuver(fromRegion, toRegion);
            var regionManeuver = (RegionManeuver) maneuver.Options.First().NextChoice;
            var battleOpportunity = (BattleOpportunityActiveNation) regionManeuver.Options.First().NextChoice;
            return battleOpportunity;
        }

        private static Maneuver NewManeuver(Region region1, Region region2)
        {
            var world = new World(new Map((region1.Identity, region2.Identity)), region1, region2);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }

        private static Maneuver NewManeuver(Region region)
        {
            var world = new World(new Map(), region);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }

        private static Region NewRegion(params Identity[] nationIdentities)
        {
            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();
            foreach (var nation in nationIdentities)
            {
                builder.Add(nation, Military.Army);
            }
            var militaryPerNation = builder.ToImmutable();

            var controllingNation = nationIdentities.Length == 1 ? nationIdentities[0] : null;
            
            return new LandRegion(RegionIdentity, militaryPerNation, controllingNation);
        }
    }
}
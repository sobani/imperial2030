﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Game;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class ProduceTest
    {
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        
        private static readonly Identity RegionIdentity = new Identity("Region");
        private static readonly Identity OtherRegionIdentity = new Identity("Other");

        [Test]
        public void CanProduceIntoArmyProvinceWithFactory()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, true);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption);
        }

        [Test]
        public void CanNotProduceIntoArmyProvinceWithoutFactory()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, false);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanProduceIntoFleetProvinceWithFactory()
        {
            var region = new FleetProvince(RegionIdentity, NationIdentity, true);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption);
        }

        [Test]
        public void CanNotProduceIntoFleetProvinceWithoutFactory()
        {
            var region = new FleetProvince(RegionIdentity, NationIdentity, false);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanNotProduceIntoArmyProvinceOfOpponent()
        {
            var region = new ArmyProvince(RegionIdentity, OpponentIdentity, false);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanNotProduceIntoFleetProvinceOfOpponent()
        {
            var region = new FleetProvince(RegionIdentity, OpponentIdentity, false);
            var produce = NewProduce(region);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanNotProduceIntoLandRegion()
        {
            var region = new LandRegion(RegionIdentity);
            var produce = NewProduce(region);
            
            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanNotProduceIntoSeaRegion()
        {
            var region = new SeaRegion(RegionIdentity);
            var produce = NewProduce(region);
            
            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOption, Is.False);
        }

        [Test]
        public void CanProduceIntoSecondProvinceWithFactory()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, true);
            var region2 = new ArmyProvince(OtherRegionIdentity, NationIdentity, true);
            var produce = NewProduce(region, region2);

            var hasOption = produce.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == OtherRegionIdentity);
            Assume.That(hasOption);
            
            var firstOption = produce.Options
                .OfType<SelectRegionOption>()
                .Single(option => option.Region == RegionIdentity);

            var hasOptionAgain = firstOption.NextChoice.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == OtherRegionIdentity);
            
            Assert.That(hasOptionAgain);
        }

        [Test]
        public void CanNotProduceTwiceIntoSameProvince()
        {
            var region = new ArmyProvince(RegionIdentity, NationIdentity, true);
            var region2 = new ArmyProvince(OtherRegionIdentity, NationIdentity, true);
            var produce = NewProduce(region, region2);

            var firstOption = produce.Options
                .OfType<SelectRegionOption>()
                .Single(option => option.Region == RegionIdentity);

            var hasOptionAgain = firstOption.NextChoice.Options
                .OfType<SelectRegionOption>()
                .Any(option => option.Region == RegionIdentity);
            
            Assert.That(hasOptionAgain, Is.False);
        }

        private static Produce NewProduce(params Region[] regions)
        {
            return new Produce(NewGame(regions), NationIdentity, new Identity("player"), CancelUndo.None);
        }

        private static Game NewGame(params Region[] regions)
        {
            var world = new World(new Map(), regions);
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            return new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
        }
    }
}
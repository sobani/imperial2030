﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Game;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class GiftTest
    {
        private static readonly Identity NationIdentity = new Identity("nation");
        private static readonly Identity PlayerIdentity = new Identity("player");
        
        [Test]
        public void GiftOnce()
        {
            var rondel = NewRondel();
            var nation = rondel.Game.GetNation(NationIdentity);
            var player = rondel.Game.GetPlayer(PlayerIdentity);
            Assume.That(rondel.Game.TryGetControllingPlayer(rondel.ActiveNationIdentity).GetValueOrDefault(), Is.EqualTo(player));

            var gift = rondel.GiftChoice;
            var selectedOption = gift.GiftOptions.SingleOrDefault();
            Assert.That(selectedOption, Is.Not.Null);

            var newGame = selectedOption.NextChoice.Game;
            var newNation = newGame.GetNation(NationIdentity);
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            Assert.That(newNation.Money, Is.EqualTo(nation.Money + 1));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money - 1));
        }

        [Test]
        public void GiftTwice()
        {
            var rondel = NewRondel();
            var nation = rondel.Game.GetNation(NationIdentity);
            var player = rondel.Game.GetPlayer(PlayerIdentity);
            Assume.That(rondel.Game.TryGetControllingPlayer(rondel.ActiveNationIdentity).GetValueOrDefault(), Is.EqualTo(player));

            var gift = rondel.GiftChoice;
            var selectedOption = gift.GiftOptions.SingleOrDefault();
            Assert.That(selectedOption, Is.Not.Null);
            var nextChoice = selectedOption.NextChoice;
            Assert.That(nextChoice, Is.TypeOf<Gift>());
            var gift2 = (Gift) nextChoice;
            var selectedOption2 = gift2.Options.SingleOrDefault(option => option.Title == "1");
            Assert.That(selectedOption2, Is.Not.Null);

            var newGame = selectedOption2.NextChoice.Game;
            var newNation = newGame.GetNation(NationIdentity);
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            Assert.That(newNation.Money, Is.EqualTo(nation.Money + 2));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money - 2));
        }

        private static RondelAction NewRondel()
        {
            var world = new World(new Map());
            var players = ImmutableList.Create(new PlayerState(PlayerIdentity, 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity), players);
            // make sure Player is the controlling player of Nation
            game = game.TryInvest(
                    PlayerIdentity,
                    NationIdentity,
                    game.GetNation(NationIdentity).Bonds.First())
                .GetValueOrThrow("test setup failed (pay for player bonds)");
            return new RondelAction(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }
    }
}
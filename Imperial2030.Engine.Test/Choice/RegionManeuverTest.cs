﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Maneuver;
using Imperial2030.Engine.Choice.Option;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class RegionManeuverTest
    {
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        
        private static readonly Identity CentralRegionIdentity = new Identity("Central");
        private static readonly Identity LandConnectionRegionIdentity = new Identity("Land");
        private static readonly Identity SingleLandConnectionRegionIdentity = new Identity("Single");
        private static readonly Identity OtherLandConnectionRegionIdentity = new Identity("Other");
        private static readonly Identity SeaRegionIdentity = new Identity("Sea");
        private static readonly Identity OtherSeaRegionIdentity = new Identity("OtterSea");
        private static readonly Identity OpponentLandRegionIdentity = new Identity("OpponentLand");

        private static readonly Map DefaultMap = new Map(
            (CentralRegionIdentity, LandConnectionRegionIdentity),
            (CentralRegionIdentity, SingleLandConnectionRegionIdentity),
            (CentralRegionIdentity, OtherLandConnectionRegionIdentity),
            (CentralRegionIdentity, SeaRegionIdentity),
            (CentralRegionIdentity, OtherSeaRegionIdentity),
            (CentralRegionIdentity, OpponentLandRegionIdentity),
            (LandConnectionRegionIdentity, SeaRegionIdentity),
            (LandConnectionRegionIdentity, OtherLandConnectionRegionIdentity),
            (SeaRegionIdentity, OtherSeaRegionIdentity));

        [Test]
        public void WhenOriginHasOnlyArmyAllOptionsAreForConnectedLandRegions()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);
            var connectedLandRegions = new[]
            {
                LandConnectionRegionIdentity,
                SingleLandConnectionRegionIdentity,
                OtherLandConnectionRegionIdentity,
                OpponentLandRegionIdentity,
            };

            var optionRegions = move.Options.OfType<SelectRegionOption>().Select(option => option.Region).ToList();
            Assert.That(optionRegions, Is.EquivalentTo(connectedLandRegions));
        }

        [Test]
        public void WhenOriginHasOnlyFleetAllOptionsAreForConnectedSeaRegions()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity, Military.Fleet);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Fleet);
            var connectedLandRegions = new[]
            {
                SeaRegionIdentity,
                OtherSeaRegionIdentity,
            };

            var optionRegions = move.Options.OfType<SelectRegionOption>().Select(option => option.Region).ToList();
            Assert.That(optionRegions, Is.EquivalentTo(connectedLandRegions));
        }

        [Test]
        public void WhenOriginHasBothArmyAndFleetAndArmyRequestedOnlyReturnsConnectedLandRegions()
        {
            var availableMilitary = new MilitaryCollection(Military.Army, Military.Fleet);
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity, availableMilitary);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);
            var connectedLandRegions = new[]
            {
                SeaRegionIdentity,
                OtherSeaRegionIdentity,
            };

            var optionRegions = move.Options.OfType<SelectRegionOption>().Select(option => option.Region).ToList();
            Assert.That(optionRegions, Is.EquivalentTo(connectedLandRegions));
        }

        [Test]
        public void WhenOriginHasBothArmyAndFleetAndFleetRequestedOnlyReturnsConnectedSeaRegions()
        {
            var availableMilitary = new MilitaryCollection(Military.Army, Military.Fleet);
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity, availableMilitary);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Fleet);
            var connectedLandRegions = new[]
            {
                SeaRegionIdentity,
                OtherSeaRegionIdentity,
            };

            var optionRegions = move.Options.OfType<SelectRegionOption>().Select(option => option.Region).ToList();
            Assert.That(optionRegions, Is.EquivalentTo(connectedLandRegions));
        }

        [Test]
        public void WhenOptionIsUnoccupiedRegionNextChoiceIsManeuver()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var maneuver = NewManeuver(regionFrom);
            Assume.That(maneuver.Game.World.Regions[LandConnectionRegionIdentity].NationsWithMilitary, Is.Empty);
            var move = new RegionManeuver(maneuver, regionFrom.Identity, Military.Army);

            var unoccupiedOption = move.Options.OfType<SelectRegionOption>().Single(option => option.Region == LandConnectionRegionIdentity);

            Assert.That(unoccupiedOption.NextChoice, Is.TypeOf<Maneuver>());
        }

        [Test]
        public void WhenOptionIsRegionWithOpponentNextChoiceIsBattleOpportunityActiveNation()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var maneuver = NewManeuver(regionFrom);
            Assume.That(maneuver.Game.World.Regions[OpponentLandRegionIdentity].NationsWithMilitary, Contains.Item(OpponentIdentity));
            var move = new RegionManeuver(maneuver, regionFrom.Identity, Military.Army);

            var occupiedOption = move.Options.OfType<SelectRegionOption>().Single(option => option.Region == OpponentLandRegionIdentity);

            Assert.That(occupiedOption.NextChoice, Is.TypeOf<BattleOpportunityActiveNation>());
        }

        [Test]
        public void SubtractsMilitaryFromOrigin()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);

            var option = move.Options.First();
            var newRegionFrom = option.NextChoice.Game.World.Regions[regionFrom.Identity];
            Assert.That(newRegionFrom.NationsWithMilitary, Is.Empty);
        }

        [Test]
        public void AddsMilitaryToDestination()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);

            var option = move.Options.OfType<SelectRegionOption>().First();
            var newRegionTo = option.NextChoice.Game.World.Regions[option.Region];
            Assert.That(newRegionTo.NationsWithMilitary, Contains.Item(NationIdentity));
        }

        [Test]
        public void AddsMilitaryToActiveNationMilitaryInDestination()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            var regionTo = NewRegion(LandConnectionRegionIdentity, NationIdentity);
            Assume.That(regionTo.MilitaryOf(NationIdentity), Has.Count.EqualTo(1));
            var move = new RegionManeuver(NewManeuver(regionFrom, regionTo), regionFrom.Identity, Military.Army);

            var occupiedOption = move.Options.OfType<SelectRegionOption>().Single(option => option.Region == regionTo.Identity);
            var newRegionTo = occupiedOption.NextChoice.Game.World.Regions[regionTo.Identity];
            Assert.That(newRegionTo.MilitaryOf(NationIdentity), Has.Count.EqualTo(2));
        }

        [Test]
        public void WhenMovingArmyToRegionWithFleetNextMoveWillOnlyConsiderFleet()
        {
            var regionFrom = NewRegion(LandConnectionRegionIdentity, NationIdentity, Military.Army);
            var fleetRegion = NewRegion(CentralRegionIdentity, NationIdentity, Military.Fleet);
            var move = new RegionManeuver(NewManeuver(regionFrom, fleetRegion), regionFrom.Identity, Military.Army);

            var occupiedOption = move.Options.OfType<SelectRegionOption>().Single(option => option.Region == fleetRegion.Identity);
            var newRegionTo = occupiedOption.NextChoice.Game.World.Regions[fleetRegion.Identity];
            Assume.That(newRegionTo.MilitaryOf(NationIdentity), Is.EquivalentTo(new[] {Military.Army, Military.Fleet}));
            Assume.That(occupiedOption.NextChoice, Is.TypeOf<Maneuver>());
            
            var nextManeuver = (Maneuver) occupiedOption.NextChoice;
            var nextRegionOption = nextManeuver.Options.OfType<SelectRegionOption>()
                .Single(option => option.Region == fleetRegion.Identity);
            Assume.That(nextRegionOption.NextChoice, Is.TypeOf<RegionManeuver>());
            var nextMove = (RegionManeuver) nextRegionOption.NextChoice;

            var nextMoveRegions = nextMove.Options.OfType<SelectRegionOption>().Select(option => option.Region);
            var connectedLandRegions = new[]
            {
                SeaRegionIdentity,
                OtherSeaRegionIdentity,
            };

            Assert.That(nextMoveRegions, Is.EquivalentTo(connectedLandRegions));
        }

        [Test]
        public void CanNotActivateMilitaryWhenNoOpponentIsPresent()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, NationIdentity);
            Assume.That(regionFrom.MilitaryOf(OpponentIdentity), Is.Empty);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);

            var activateOptionCount = move.Options.OfType<ActivateOption>().Count();
            Assert.That(activateOptionCount, Is.Zero);
        }

        [Test]
        public void CanActivateMilitaryWhenOpponentIsPresent()
        {
            var regionFrom = NewRegion(CentralRegionIdentity, (NationIdentity, Military.Army), (OpponentIdentity, Military.Army));
            Assume.That(regionFrom.MilitaryOf(OpponentIdentity), Is.Not.Empty);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);

            var activateOptionCount = move.Options.OfType<ActivateOption>().Count();
            Assert.That(activateOptionCount, Is.EqualTo(1));
        }

        [Test]
        public void CanActivateArmyWhenBothTypesOfMilitaryArePresent()
        {
            var military = new MilitaryCollection(Military.Army, Military.Army, Military.Fleet);
            var regionFrom = NewRegion(CentralRegionIdentity, (NationIdentity, military), (OpponentIdentity, Military.Army));
            Assume.That(regionFrom.MilitaryOf(OpponentIdentity), Is.Not.Empty);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Army);

            var activateOptions = move.Options.OfType<ActivateOption>();
            var activateMilitary = activateOptions.Select(ao => ao.Military);
            Assert.That(activateMilitary, Is.EquivalentTo(new[] {Military.Army}));
        }
        
        [Test]
        public void CanActivateFleetWhenBothTypesOfMilitaryArePresent()
        {
            var military = new MilitaryCollection(Military.Army, Military.Army, Military.Fleet);
            var regionFrom = NewRegion(CentralRegionIdentity, (NationIdentity, military), (OpponentIdentity, Military.Army));
            Assume.That(regionFrom.MilitaryOf(OpponentIdentity), Is.Not.Empty);
            var move = new RegionManeuver(NewManeuver(regionFrom), regionFrom.Identity, Military.Fleet);

            var activateOptions = move.Options.OfType<ActivateOption>();
            var activateMilitary = activateOptions.Select(ao => ao.Military);
            Assert.That(activateMilitary, Is.EquivalentTo(new[] {Military.Fleet}));
        }

        private static Region NewRegion(Identity regionIdentity, Identity nation, Military military = Military.Army)
        {
            return new LandRegion(regionIdentity, RegionMilitary(nation, military), nation);
        }

        private static Region NewRegion(Identity regionIdentity, Identity nation, MilitaryCollection military)
        {
            return new LandRegion(regionIdentity, RegionMilitary(nation, military), nation);
        }

        private static Region NewRegion(Identity regionIdentity, params (Identity nation, MilitaryCollection military)[] militaries)
        {
            return new LandRegion(regionIdentity, RegionMilitary(militaries), null);
        }

        private static ImmutableDictionary<Identity, MilitaryCollection> RegionMilitary(Identity nation, MilitaryCollection military)
        {
            return ImmutableDictionary<Identity, MilitaryCollection>.Empty.Add(nation, military);
        }

        private static ImmutableDictionary<Identity, MilitaryCollection> RegionMilitary(params (Identity nation, MilitaryCollection military)[] militaries)
        {
            var builder = ImmutableDictionary<Identity, MilitaryCollection>.Empty.ToBuilder();
            foreach (var (nation, military) in militaries)
            {
                builder.Add(nation, military);
            }
            return builder.ToImmutable();
        }

        private static Maneuver NewManeuver(params Region[] regions)
        {
            var defaultRegions = new Region[]
            {
                new SeaRegion(SeaRegionIdentity),
                new SeaRegion(OtherSeaRegionIdentity),
                new LandRegion(OpponentLandRegionIdentity, RegionMilitary(OpponentIdentity, Military.Army), null),
            };
            var allRegions = defaultRegions.Concat(regions);
            
            var world = new World(DefaultMap, allRegions.ToArray());
            var players = ImmutableList.Create(new PlayerState(new Identity("player"), 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity, OpponentIdentity), players);
            return new Maneuver(game, NationIdentity, PlayerIdentity, CancelUndo.None);
        }
    }
}
﻿using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using NUnit.Framework;

namespace Imperial2030.Engine.Test.Choice
{
    public class InvestorTest
    {
        private static readonly Identity NationIdentity = new Identity("nation");
        private static readonly Identity PlayerIdentity = new Identity("player");
        private static readonly Identity OpponentIdentity = new Identity("opponent");

        [Test]
        public void NationHasEnoughMoneyForAllPlayers()
        {
            var game = NewGame();
            var player = game.GetPlayer(PlayerIdentity);
            var opponent = game.GetPlayer(OpponentIdentity);
            var nation = game.GetNation(NationIdentity);
            var playerInterest = player.GetBondTotal(NationIdentity).Interest;
            var opponentInterest = opponent.GetBondTotal(NationIdentity).Interest;
            var investorPayout = playerInterest + opponentInterest;
            Assume.That(nation.Money, Is.GreaterThanOrEqualTo(investorPayout));
            
            var investor = new Engine.Choice.Game.Investor(game, NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var selectedOption = investor.Options
                .SingleOrDefault(option => option.Title == "Distribute");
            Assert.That(selectedOption, Is.Not.Null);
            
            var newGame = selectedOption.NextChoice.Game;
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            var newOpponent = newGame.GetPlayer(OpponentIdentity);
            var newNation = newGame.GetNation(NationIdentity);
            Assert.That(newNation.Money, Is.EqualTo(nation.Money - investorPayout));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money + playerInterest));
            Assert.That(newOpponent.Money, Is.EqualTo(opponent.Money + opponentInterest));
        }

        [Test]
        public void NationHasEnoughMoneyOnlyForOpponents()
        {
            var game = NewGame(nationMoney: 4);
            var player = game.GetPlayer(PlayerIdentity);
            var opponent = game.GetPlayer(OpponentIdentity);
            var nation = game.GetNation(NationIdentity);
            var playerInterest = player.GetBondTotal(NationIdentity).Interest;
            var opponentInterest = opponent.GetBondTotal(NationIdentity).Interest;
            var investorPayout = playerInterest + opponentInterest;
            Assume.That(nation.Money, Is.GreaterThanOrEqualTo(opponentInterest));
            Assume.That(nation.Money, Is.Not.GreaterThanOrEqualTo(investorPayout));
            
            var investor = new Engine.Choice.Game.Investor(game, NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var selectedOption = investor.Options
                .SingleOrDefault(option => option.Title == "Distribute");
            Assert.That(selectedOption, Is.Not.Null);
            
            var newGame = selectedOption.NextChoice.Game;
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            var newOpponent = newGame.GetPlayer(OpponentIdentity);
            var newNation = newGame.GetNation(NationIdentity);
            Assert.That(newNation.Money, Is.EqualTo(0));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money + nation.Money - opponentInterest));
            Assert.That(newOpponent.Money, Is.EqualTo(opponent.Money + opponentInterest));
        }

        [Test]
        public void NationHasEnoughMoneyOnlyForOpponentsAndPlayerDoesNotHaveEnoughMoneyToCoverHisOwnShortfall()
        {
            // 16 for the amount needed to pay for the players bonds
            const int playerMoney = 16 + 1;
            var game = NewGame(nationMoney: 4, playerMoney);
            var player = game.GetPlayer(PlayerIdentity);
            var opponent = game.GetPlayer(OpponentIdentity);
            var nation = game.GetNation(NationIdentity);
            var playerInterest = player.GetBondTotal(NationIdentity).Interest;
            var opponentInterest = opponent.GetBondTotal(NationIdentity).Interest;
            var investorPayout = playerInterest + opponentInterest;
            Assume.That(nation.Money, Is.GreaterThanOrEqualTo(opponentInterest));
            Assume.That(nation.Money, Is.Not.GreaterThanOrEqualTo(investorPayout));
            Assume.That(nation.Money + player.Money, Is.Not.GreaterThanOrEqualTo(investorPayout));
            
            var investor = new Engine.Choice.Game.Investor(game, NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var selectedOption = investor.Options
                .SingleOrDefault(option => option.Title == "Distribute");
            Assert.That(selectedOption, Is.Not.Null);
            
            var newGame = selectedOption.NextChoice.Game;
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            var newOpponent = newGame.GetPlayer(OpponentIdentity);
            var newNation = newGame.GetNation(NationIdentity);
            Assert.That(newNation.Money, Is.EqualTo(0));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money + nation.Money - opponentInterest));
            Assert.That(newOpponent.Money, Is.EqualTo(opponent.Money + opponentInterest));
        }

        [Test]
        public void NationDoesNotHaveEnoughMoneyForOpponents()
        {
            var game = NewGame(nationMoney: 2);
            var player = game.GetPlayer(PlayerIdentity);
            var opponent = game.GetPlayer(OpponentIdentity);
            var nation = game.GetNation(NationIdentity);
            var opponentInterest = opponent.GetBondTotal(NationIdentity).Interest;
            Assume.That(nation.Money, Is.Not.GreaterThanOrEqualTo(opponentInterest));
            
            var investor = new Engine.Choice.Game.Investor(game, NationIdentity, PlayerIdentity, CancelUndo.None);
            
            var selectedOption = investor.Options
                .SingleOrDefault(option => option.Title == "Distribute");
            Assert.That(selectedOption, Is.Not.Null);
            
            var newGame = selectedOption.NextChoice.Game;
            var newPlayer = newGame.GetPlayer(PlayerIdentity);
            var newOpponent = newGame.GetPlayer(OpponentIdentity);
            var newNation = newGame.GetNation(NationIdentity);
            Assert.That(newNation.Money, Is.EqualTo(0));
            Assert.That(newPlayer.Money, Is.EqualTo(player.Money + nation.Money - opponentInterest));
            Assert.That(newOpponent.Money, Is.EqualTo(opponent.Money + opponentInterest));
        }

        /// <param name="nationMoney">at least 8 to pay all players fully</param>
        /// <param name="controllingPlayerMoney">at least 16 to pay for the bonds</param>
        private static Game NewGame(int nationMoney = 10, int controllingPlayerMoney = 42)
        {
            var world = new World(new Map());
            var players = ImmutableList.Create(new PlayerState(PlayerIdentity, controllingPlayerMoney), new PlayerState(OpponentIdentity, 42));
            var game = new Game(world, ImmutableList.Create(NationIdentity), players);
            // make sure Player and Opponent have bonds and Player is the controlling player of Nation
            return game
                .TryInvest(
                    PlayerIdentity,
                    NationIdentity,
                    game.GetNation(NationIdentity).Bonds[5])
                .GetValueOrThrow("test setup failed (pay for player bonds)")
                .TryInvest(
                    OpponentIdentity,
                    NationIdentity,
                    game.GetNation(NationIdentity).Bonds[3])
                .GetValueOrThrow("test setup failed (pay for opponent bonds)")
                .WithNationMoney(NationIdentity, nationMoney)
                .GetValueOrThrow("test setup failed (give nation money)");
        }
    }
}
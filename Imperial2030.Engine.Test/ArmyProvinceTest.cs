﻿using System.Collections.Immutable;
using System.Linq;
using NUnit.Framework;

namespace Imperial2030.Engine.Test
{
    public class ArmyProvinceTest
    {
        private const string NationName = "nation";
        private const string OpponentName = "opponent";
        private const string ProvinceName = "province";
        private static readonly Identity NationIdentity = new Identity(NationName);
        private static readonly Identity OpponentIdentity = new Identity(OpponentName);
        private static readonly Identity ProvinceIdentity = new Identity(ProvinceName);

        [Test]
        public void NewProvinceHasNoMilitary()
        {
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, false);
            
            Assert.That(province.NationsWithMilitary, Is.Empty);
        }

        [TestCase(false, NationName, true, false)]
        [TestCase(false, OpponentName, false, true)]
        [TestCase(true, NationName, true, true)]
        public void WithFactory(bool startsWithFactory, string activeNation, bool endsWithFactory, bool resultIsSame)
        {
            var activeIdentity = new Identity(activeNation);
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, startsWithFactory);
            
            var newRegion = province.WithFactory(activeIdentity);

            if (resultIsSame)
            {
                Assert.That(newRegion.HasValue, Is.False);
            }
            else
            {
                Assert.That(newRegion.HasValue);
                Assert.That(newRegion.Cast<ArmyProvince>().TryGetValue(out var newProvince), "new region is not an ArmyProvince");
                Assert.That(newProvince.HasFactory, Is.EqualTo(endsWithFactory));
                Assert.That(newProvince, Is.Not.SameAs(province));
            }
        }

        [TestCase(true, OpponentName, 3, false, false)]
        [TestCase(false, OpponentName, 3, false, true)]
        [TestCase(true, OpponentName, 2, false, true)]
        [TestCase(true, NationName, 3, false, true)]
        [TestCase(true, OpponentName, 3, true, true)]
        public void DestroyFactory(
            bool hasFactory,
            string activeNation,
            int armyCount,
            bool hasDefence,
            bool resultIsSame)
        {
            var activeIdentity = new Identity(activeNation);
            var militaryPerNation = ImmutableDictionary.CreateBuilder<Identity, MilitaryCollection>();
            militaryPerNation[activeIdentity] = new MilitaryCollection(Enumerable.Repeat(Military.Army, armyCount).ToImmutableList());
            if (hasDefence)
                militaryPerNation[NationIdentity] = new MilitaryCollection(Military.Army);
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, militaryPerNation.ToImmutable(), hasFactory);

            var newRegion = province.DestroyFactory(activeIdentity);
            if (resultIsSame)
            {
                Assert.That(newRegion.HasValue, Is.False);
            }
            else
            {
                Assert.That(newRegion.HasValue);
                Assert.That(newRegion.Cast<ArmyProvince>().TryGetValue(out var newProvince), "new region is not an ArmyProvince");
                Assert.That(newProvince.HasFactory, Is.False);
            }
        }

        [TestCase(3, 0)]
        [TestCase(4, 1)]
        public void DestroyFactoryEliminatesThreeArmyFromAggressor(
            int startingArmyCount,
            int endingArmyCount)
        {
            var militaryPerNation = ImmutableDictionary.CreateBuilder<Identity, MilitaryCollection>();
            militaryPerNation[OpponentIdentity] = new MilitaryCollection(Enumerable.Repeat(Military.Army, startingArmyCount).ToImmutableList());
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, militaryPerNation.ToImmutable(), true);
            Assume.That(province.MilitaryOf(OpponentIdentity), Has.Count.EqualTo(startingArmyCount));

            var newRegion = province.DestroyFactory(OpponentIdentity);
            Assert.That(newRegion.TryGetValue(out var nr));
            Assert.That(nr.MilitaryOf(OpponentIdentity), Has.Count.EqualTo(endingArmyCount));
        }

        [Test]
        public void WithProduceWhenHasFactoryReturnsNewProvinceWithArmy()
        {
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, true);
            Assume.That(province.HasFactory);
            Assume.That(province.MilitaryOf(NationIdentity), Is.Empty);
            
            var newRegion = province.WithProduce(NationIdentity);
            Assert.That(newRegion.TryGetValue(out var nr));
            Assert.That(nr.MilitaryOf(NationIdentity), Is.EquivalentTo(new[] {Military.Army}));
        }

        [Test]
        public void WithProduceWhenNotHasFactoryReturnsMaybeNothing()
        {
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, false);
            Assume.That(province.HasFactory, Is.False);
            
            var newRegion = province.WithProduce(NationIdentity);
            Assert.That(newRegion.HasValue, Is.False);
        }

        [Test]
        public void WithProduceWithOpponentReturnsMaybeNothing()
        {
            var province = new ArmyProvince(ProvinceIdentity, NationIdentity, true);
            Assume.That(province.HasFactory, Is.True);
            
            var newRegion = province.WithProduce(OpponentIdentity);
            Assert.That(newRegion.HasValue, Is.False);
        }
    }
}
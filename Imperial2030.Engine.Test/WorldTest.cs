﻿using System.Collections.Immutable;
using NUnit.Framework;

namespace Imperial2030.Engine.Test
{
    public class WorldTest
    {
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity OpponentIdentity = new Identity("Europe");
        private static readonly Identity ProvinceIdentity = new Identity("Chicago");
        private static readonly Identity RegionIdentity = new Identity("Canada");
        private static readonly Map Map = new Map((ProvinceIdentity, RegionIdentity));
        
        [Test]
        public void MoveMilitaryBetweenRegionsWithoutOpponent()
        {
            var world = new World(Map, new LandRegion(ProvinceIdentity, RegionMilitary(NationIdentity, Military.Army), NationIdentity));
            var origin = world.Regions[ProvinceIdentity];
            var destination = world.Regions[RegionIdentity];
            Assume.That(origin.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(destination.NationsWithMilitary, Is.Empty);

            var newWorld = world.MoveMilitary(NationIdentity, ProvinceIdentity, RegionIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to move military)");

            var newOrigin = newWorld.Regions[ProvinceIdentity];
            var newDestination = newWorld.Regions[RegionIdentity];
            Assert.That(newOrigin.NationsWithMilitary, Is.Empty);
            Assume.That(newDestination.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
        }

        [Test]
        public void MoveMilitaryBetweenRegionsToOpponent()
        {
            var world = new World(
                Map,
                new LandRegion(ProvinceIdentity, RegionMilitary(NationIdentity, Military.Army), NationIdentity),
                new LandRegion(RegionIdentity, RegionMilitary(OpponentIdentity, Military.Army), OpponentIdentity));
            var origin = world.Regions[ProvinceIdentity];
            var destination = world.Regions[RegionIdentity];
            Assume.That(origin.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(destination.MilitaryOf(OpponentIdentity), Is.Not.Empty);

            var newWorld = world.MoveMilitary(NationIdentity, ProvinceIdentity, RegionIdentity, Military.Army)
                .GetValueOrThrow("test setup failed (unable to move military)");

            var newOrigin = newWorld.Regions[ProvinceIdentity];
            var newDestination = newWorld.Regions[RegionIdentity];
            Assert.That(newOrigin.NationsWithMilitary, Is.Empty);
            Assume.That(newDestination.MilitaryOf(NationIdentity).Count, Is.EqualTo(1));
            Assume.That(newDestination.MilitaryOf(OpponentIdentity).Count, Is.EqualTo(1));
        }

        private static ImmutableDictionary<Identity, MilitaryCollection> RegionMilitary(Identity nation, Military military)
        {
            return ImmutableDictionary<Identity, MilitaryCollection>.Empty.Add(nation, military);
        }
    }
}
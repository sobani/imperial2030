﻿using System.Collections.Immutable;
using NUnit.Framework;

namespace Imperial2030.Engine.Test
{
    public class LandRegionTest
    {
        private static readonly Identity NationIdentity = new Identity("USA");
        private static readonly Identity RegionIdentity = new Identity("Canada");

        [Test]
        public void NewRegionHasNoMilitary()
        {
            var region = new LandRegion(RegionIdentity);
            
            Assert.That(region.NationsWithMilitary, Is.Empty);
        }

        [Test]
        public void WithFactorReturnsMaybeNothing()
        {
            var region = new LandRegion(RegionIdentity, ImmutableDictionary<Identity, MilitaryCollection>.Empty, NationIdentity);
            
            var newRegion = region.WithFactory(NationIdentity);
            Assert.That(newRegion.HasValue, Is.False);
        }

        [Test]
        public void WithProduceReturnsMaybeNothing()
        {
            var region = new LandRegion(RegionIdentity, ImmutableDictionary<Identity, MilitaryCollection>.Empty, NationIdentity);
            
            var newRegion = region.WithProduce(NationIdentity);
            Assert.That(newRegion.HasValue, Is.False);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;

namespace Imperial2030.Engine
{
    public sealed class PlayerInfo
    {
        private readonly PlayerState _state;
        private readonly Game _game;

        public PlayerInfo(
            PlayerState state,
            Game game)
        {
            _state = state;
            _game = game;
        }

        public Identity Identity => _state.Identity;

        public string Name => _state.Name;

        public int Money => _state.Money;

        public IEnumerable<Bond> BondTotalPerNation => _state.BondTotalPerNation;

        public Bond GetBondTotal(Identity nation) => _state.GetBondTotal(nation);

        public IEnumerable<Bond> GetBonds(Identity nation) => _state.GetBonds(nation);

        public int Score => Money + _state.Bonds.Sum(GetBondScore);

        private int GetBondScore(Bond bond)
        {
            return _game.GetNation(bond.Nation).PowerFactor * bond.Interest;
        }
    }
}
﻿using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class World
    {
        public World(Map map, params Region[] regions)
        {
            Map = map;

            var regionsBuilder = regions.ToImmutableDictionary(region => region.Identity).ToBuilder();
            foreach (var mapRegion in map.Regions)
            {
                if (!regionsBuilder.ContainsKey(mapRegion))
                    regionsBuilder[mapRegion] = new LandRegion(mapRegion);
            }

            Regions = regionsBuilder.ToImmutable();
        }

        private World(Map map, ImmutableDictionary<Identity, Region> regions)
        {
            Map = map;
            Regions = regions;
        }

        public Map Map { get; }
        
        public ImmutableDictionary<Identity, Region> Regions { get; }

        [Pure]
        public Maybe<World> MoveMilitary(
            Identity nation,
            Identity regionFrom,
            Identity regionTo,
            Military military)
        {
            var newRegionFrom = Regions[regionFrom].MoveMilitaryOut(nation, military);
            var newRegionTo = Regions[regionTo].MoveMilitaryIn(nation, military);
            
            return WithRegion(newRegionFrom)
                .Select(w => w.WithRegion(newRegionTo));
        }

        public Maybe<World> Battle(Identity region, Identity activeNation, Military activeNationMilitary, Identity inactiveNation, Military inactiveNationMilitary)
        {
            var newRegion = Regions[region]
                .Battle(activeNation, activeNationMilitary, inactiveNation, inactiveNationMilitary);
            return WithRegion(newRegion);
        }

        [Pure]
        public Maybe<World> DestroyFactory(Identity region, Identity activeNation)
        {
            return WithRegion(
                Regions[region].DestroyFactory(activeNation));
        }

        [Pure]
        public Maybe<World> WithRegion(Region region)
        {
            var newRegions = Regions.SetItem(region.Identity, region);
            return newRegions == Regions
                ? default(Maybe<World>)
                : new World(Map, newRegions);
        }

        [Pure]
        public Maybe<World> WithRegion(Maybe<Region> region)
        {
            return region.Select(WithRegion);
        }
    }
}
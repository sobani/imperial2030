﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class ArmyProvince : Region
    {
        public ArmyProvince(Identity identity, Identity nation, bool hasFactory)
            : this(identity, nation, ImmutableDictionary<Identity, MilitaryCollection>.Empty, hasFactory)
        {
        }

        public ArmyProvince(
            Identity identity,
            Identity nation,
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            bool hasFactory)
            : base(identity, militaryPerNation, null)
        {
            HasFactory = hasFactory;
            Nation = nation;
        }

        [Pure]
        protected override Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
        {
            return new ArmyProvince(Identity, Nation, militaryPerNation, HasFactory);
        }

        public bool HasFactory { get; }

        public Identity Nation { get; }

        public override int TaxFor(Identity nation)
        {
            return nation == Nation && HasFactory && !HasOpposingMilitary(nation)
                ? 2
                : 0;
        }

        public override Maybe<Region> WithProduce(Identity nation)
        {
            if (HasFactory && nation == Nation)
                return AddMilitary(Nation, Military.Army);
            
            return default;
        }

        [Pure]
        public override IEnumerable<(Military, Region newRegion)> ImportPossibilities(Identity nation)
        {
            if (nation != Nation)
                yield break;
            
            yield return (Military.Army, AddMilitary(nation, Military.Army));
        }

        [Pure]
        public override Military? TryGetFactory()
        {
            return HasFactory ? Military.Army : (Military?) null;
        }

        [Pure]
        public override Maybe<Region> WithFactory(Identity nation)
        {
            if (!HasFactory && nation == Nation)
                return new ArmyProvince(Identity, Nation, MilitaryPerNation, true);

            return default;
        }

        public override Maybe<Region> DestroyFactory(Identity nation)
        {
            if (!HasFactory || nation == Nation)
                return default;

            if (!MilitaryOf(Nation).IsEmpty)
                return default;

            return new ArmyProvince(Identity, Nation, MilitaryPerNation, false)
                .MoveMilitaryOut(nation, Military.Army)
                .Select(nr => nr.MoveMilitaryOut(nation, Military.Army))
                .Select(nr => nr.MoveMilitaryOut(nation, Military.Army));
        }

        public override Military AcceptableMilitary => Military.Army;

        public override bool HasRail(Identity nation)
        {
            return nation == Nation && !HasOpposingMilitary(nation);
        }
    }
}
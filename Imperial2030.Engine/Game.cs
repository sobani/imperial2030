﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Choice.Game;
using Imperial2030.Engine.Configuration;
using Imperial2030.Engine.Utils;
using MoreLinq;

namespace Imperial2030.Engine
{
    public sealed class Game
    {
        private readonly int _currentNationIndex;
        private readonly ImmutableList<PlayerState> _players;

        public Game(
            World world,
            ImmutableList<Identity> nations,
            ImmutableList<PlayerState> players)
        {
            _currentNationIndex = 0;
            _players = players;
            World = world;
            Nations = nations.ConvertAll(Nation.New);
            ControllingPlayers = ImmutableDictionary<Identity, Identity>.Empty;
        }

        private Game(
            World world,
            ImmutableList<Nation> nations,
            int currentNationIndex,
            ImmutableList<PlayerState> players,
            ImmutableDictionary<Identity, Identity> controllingPlayers)
        {
            if (currentNationIndex < 0 || nations.Count <= currentNationIndex)
            {
                throw new ArgumentOutOfRangeException(
                    nameof(currentNationIndex),
                    currentNationIndex,
                    "should be between 0 and " + (nations.Count - 1));
            }
            
            _currentNationIndex = currentNationIndex;
            World = world;
            Nations = nations;
            _players = players;
            ControllingPlayers = controllingPlayers;
        }

        public World World { get; }
        public ImmutableList<Nation> Nations { get; }
        public IEnumerable<PlayerInfo> Players => _players.Select(state => new PlayerInfo(state, this));
        public ImmutableDictionary<Identity, Identity> ControllingPlayers { get; }

        public Nation CurrentNation => Nations[_currentNationIndex];

        public Nation GetNation(Identity nationIdentity) => Nations.Single(n => n.Identity == nationIdentity);

        public PlayerState GetPlayer(Identity playerIdentity) => _players.Single(p => p.Identity == playerIdentity);

        public Maybe<PlayerState> TryGetControllingPlayer(Identity nationIdentity) =>
            ControllingPlayers.TryGet(nationIdentity).Select(GetPlayer);

        public Identity GetControllingPlayerIdentityOrThrow(Identity nationIdentity) =>
            ControllingPlayers.TryGet(nationIdentity).GetValueOrThrow($"{nationIdentity} does not have a controlling player");

        [Pure]
        public RondelAction FirstChoice()
        {
            return GetRondelChoice(CancelUndo.None);
        }

        private RondelAction GetRondelChoice(CancelUndo cancelUndo)
        {
            var currentNationIdentity = CurrentNation.Identity;
            var activePlayerIdentity = GetControllingPlayerIdentityOrThrow(currentNationIdentity);
            return new RondelAction(this, currentNationIdentity, activePlayerIdentity, cancelUndo);
        }

        [Pure]
        public IChoice NextChoice(CancelUndo cancelUndo)
        {
            if (Nations.Any(nation => nation.Power >= Constants.MaxNationPower))
            {
                return new RestartChoice(this);
            }

            static IChoice RondelChoiceFactory(Game game, CancelUndo cancelUndo) => game.GetRondelChoice(cancelUndo);

            var biddingNation = CurrentNation.Identity;
            var firstBidder = GetControllingPlayerIdentityOrThrow(biddingNation);
            var firstBidderIndex = _players.FindIndex(player => player.Identity == firstBidder);

            var nextChoiceFactory = GetBiddingChoiceFactory(biddingNation, firstBidderIndex, RondelChoiceFactory);

            var nextNationIndex = (_currentNationIndex + 1)%Nations.Count; 
            var newGame = new Game(World, Nations, nextNationIndex, _players, ControllingPlayers);
            return nextChoiceFactory(newGame, cancelUndo);
        }

        [Pure]
        public IChoice InvestingStartChoice()
        {
            Func<Game, CancelUndo, IChoice> nextChoiceFactory = (game, cancelUndo) => game.GetRondelChoice(cancelUndo);

            // reverse
            for (int nationIndex = Nations.Count - 1; nationIndex >= 0; nationIndex--)
            {
                var biddingNation = Nations[nationIndex].Identity;
                var firstBidderIndex = nationIndex%_players.Count;
                
                nextChoiceFactory = GetBiddingChoiceFactory(biddingNation, firstBidderIndex, nextChoiceFactory);
            }
            
            return nextChoiceFactory(this, CancelUndo.None);
        }

        private Func<Game, CancelUndo, IChoice> GetBiddingChoiceFactory(
            Identity biddingNation,
            int firstBidderIndex,
            Func<Game, CancelUndo, IChoice> nextChoiceFactory)
        {
            // reverse
            for (var j = _players.Count - 1; j >= 0; j--)
            {
                var bidderIndex = (firstBidderIndex + j)%_players.Count;
                var bidder = _players[bidderIndex].Identity;

                var prevChoiceFactory = nextChoiceFactory;
                nextChoiceFactory = (game, cancelUndo) => new Invest(game, bidder, biddingNation, prevChoiceFactory, cancelUndo);
            }

            return nextChoiceFactory;
        }

        public Maybe<Game> WithFactory(Identity regionIdentity, Identity nationIdentity)
        {
            var region = World.Regions[regionIdentity];
            var newRegion = region.WithFactory(nationIdentity);
            var newWorld = World.WithRegion(newRegion);

            var nation = GetNation(nationIdentity);
            var newNation = nation.PayForFactory();
            
            var newNations = Nations.TryReplace(nation, newNation);

            return (newWorld, newNations).Join((nw, nn) => new Game(nw, nn, _currentNationIndex, _players, ControllingPlayers));
        }

        public Maybe<Game> WithInvestor(Identity nationIdentity, Investor investor)
        {
            var nation = GetNation(nationIdentity);
            var newNation = nation.WithMoney(nation.Money - investor.TotalInterest + investor.MissingMoney);
            var newNations = Nations.TryReplace(nation, newNation);

            Maybe<ImmutableList<PlayerState>> newPlayers = _players;

            foreach (var (playerIdentity, interest) in investor.PlayerInterest)
            {
                newPlayers = newPlayers.TryUpdate(
                    player => player.Identity == playerIdentity,
                    player => player.WithMoney(player.Money + interest));
            }

            if (investor.MissingMoney > 0)
            {
                newPlayers =
                    from controllingPlayerIdentity in ControllingPlayers.TryGet(nationIdentity)
                    from np in newPlayers
                    select np.TryUpdate(
                        p => p.Identity == controllingPlayerIdentity,
                        p => p.WithMoney(p.Money - investor.MissingMoney));
            }

            return Wrap(newNations, newPlayers);
        }

        public Maybe<Game> WithTax(Identity nationIdentity)
        {
            return GetNation(nationIdentity)
                .WithTax(Tax.Calculate(World, nationIdentity))
                .Select(
                    (newNation, successBonus) =>
                    {
                        var newControllingPlayer = TryGetControllingPlayer(nationIdentity)
                            .Select(cp => cp.WithMoney(cp.Money + successBonus));

                        return Wrap(
                            Nations.TryReplace(GetNation(nationIdentity), newNation),
                            _players.TryReplace(newControllingPlayer, player => player.Identity));
                    });
        }

        public Maybe<Game> WithNationMoney(Identity nationIdentity, int newNationMoney)
        {
            var nation = GetNation(nationIdentity);
            var newNation = nation.WithMoney(newNationMoney);
            
            return Wrap( 
                Nations.TryReplace(nation, newNation));
        }

        public Maybe<Game> WithNation(Maybe<Nation> newNation)
        {
            return Wrap(
                Nations.TryReplace(newNation, n => n.Identity));
        }

        public Maybe<Game> WithGift(PlayerState player, Identity nationIdentity, int amount)
        {
            var newNations = Nations.TryUpdate(
                nat => nat.Identity == nationIdentity,
                nat => nat.WithMoney(nat.Money + amount));

            var newPlayers = _players.TryReplace(player, player.WithMoney(player.Money - amount));
            
            return Wrap(newNations, newPlayers);
        }

        public Maybe<Game> WithPlayerCost(Identity playerIdentity, int amount)
        {
            var player = GetPlayer(playerIdentity);

            var newPlayers = _players.TryReplace(player, player.WithMoney(player.Money - amount));

            return newPlayers.Select(players => new Game(World, Nations, _currentNationIndex, players, ControllingPlayers));
        }

        [Pure]
        public Maybe<Game> WithWorld(World world)
        {
            if (ReferenceEquals(World, world))
                return default;
            
            return new Game(world, Nations, _currentNationIndex, _players, ControllingPlayers);
        }

        [Pure]
        public Maybe<Game> WithRegion(Region region)
        {
            return World.WithRegion(region)
                .Select(WithWorld);
        }

        [Pure]
        public Maybe<Game> TryInvest(Identity playerIdentity, Identity nationIdentity, Bond bond)
        {
            var player = GetPlayer(playerIdentity);
            var nation = GetNation(nationIdentity);
            var newNation = nation.SellBond(bond);
            var newPlayer = player.AcquireBond(bond);
            
            var newNations = Nations.TryReplace(nation, newNation);
            var newPlayers = _players.TryReplace(player, newPlayer);

            return (newNations, newPlayers).Join(WithNationsAndPlayers);
        }

        [Pure]
        public Maybe<Game> TryInvest(Identity playerIdentity, Identity nationIdentity, Bond newBond, Bond oldBond)
        {
            var player = GetPlayer(playerIdentity);
            var nation = GetNation(nationIdentity);
            var newNation = nation.SellBond(newBond, oldBond);
            var newPlayer = player.AcquireBond(newBond, oldBond);
            
            var newNations = Nations.TryReplace(nation, newNation);
            var newPlayers = _players.TryReplace(player, newPlayer);

            return (newNations, newPlayers).Join(WithNationsAndPlayers);
        }

        private Game WithNationsAndPlayers(ImmutableList<Nation> newNations, ImmutableList<PlayerState> newPlayers)
        {
            var newControllingPlayers = ControllingPlayers.ToBuilder();

            var maxPlayerBondTotalPerNation = newPlayers
                .SelectMany(
                    player => player.BondTotalPerNation,
                    (player, bondTotal) => (nation: bondTotal.Nation, player, totalValue: bondTotal.Value))
                .GroupBy(
                    playerBondTotal => playerBondTotal.nation,
                    (nation, playerBondTotals) => playerBondTotals.MaxBy(playerBondTotal => playerBondTotal.totalValue).First());

            foreach (var (nationIdentity, maxPlayer, maxValue) in maxPlayerBondTotalPerNation)
            {
                var newControllingPlayer =
                    TryGetControllingPlayer(nationIdentity)
                        .Where(controllingPlayer => maxValue <= controllingPlayer.GetBondTotal(nationIdentity).Value)
                        .OrElse(maxPlayer);
                
                newControllingPlayers[nationIdentity] = newControllingPlayer.Identity;
            }

            return new Game(World, newNations, _currentNationIndex, newPlayers, newControllingPlayers.ToImmutable());
        }

        private Maybe<Game> Wrap(Maybe<ImmutableList<Nation>> newNations)
        {
            return newNations.Select(nn => new Game(World, nn, _currentNationIndex, _players, ControllingPlayers));
        }

        private Maybe<Game> Wrap(
            Maybe<ImmutableList<Nation>> newNations,
            Maybe<ImmutableList<PlayerState>> newPlayers)
        {
            return (newNations, newPlayers).Join(
                (nn, np) => new Game(World, nn, _currentNationIndex, np, ControllingPlayers));
        }
    }
}
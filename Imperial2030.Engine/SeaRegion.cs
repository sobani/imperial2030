﻿using System.Collections.Immutable;
using System.Diagnostics.Contracts;

namespace Imperial2030.Engine
{
    public sealed class SeaRegion : Region
    {
        public SeaRegion(Identity identity)
            : base(identity)
        {
        }

        public SeaRegion(
            Identity identity,
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
            : base(identity, militaryPerNation, controllingNation)
        {
        }

        [Pure]
        protected override Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
        {
            return new SeaRegion(Identity, militaryPerNation, controllingNation);
        }

        public override Military AcceptableMilitary => Military.Fleet;
    }
}
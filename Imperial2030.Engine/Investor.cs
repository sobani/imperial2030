﻿using System;
using System.Collections.Immutable;
using System.Linq;

namespace Imperial2030.Engine
{
    public sealed class Investor
    {
        private Investor(
            ImmutableDictionary<Identity, int> playerInterest,
            int missingMoney)
        {
            PlayerInterest = playerInterest;
            MissingMoney = missingMoney;
        }
        
        public static Investor Calculate(Game game, Identity nationIdentity)
        {
            var playerInterests = game.Players
                .Select(player => (player, bondTotal: player.GetBondTotal(nationIdentity)))
                .ToImmutableDictionary(tuple => tuple.player.Identity, tuple => tuple.bondTotal.Interest);
            
            var totalInterest = playerInterests.Values.Sum();
            var nation = game.GetNation(nationIdentity);
            var missingMoney = Math.Max(0, totalInterest - nation.Money);
            
            return new Investor(playerInterests, missingMoney);
        }
     
        public ImmutableDictionary<Identity, int> PlayerInterest { get; }
        public int MissingMoney { get; }

        public int TotalInterest => PlayerInterest.Values.Sum();
    }
}
﻿using System.Collections.Immutable;
using System.Diagnostics.Contracts;

namespace Imperial2030.Engine
{
    public sealed class LandRegion : Region
    {
        public LandRegion(Identity identity)
            : base(identity)
        {
        }

        public LandRegion(
            Identity identity,
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
            : base(identity, militaryPerNation, controllingNation)
        {
        }

        [Pure]
        protected override Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
        {
            return new LandRegion(Identity, militaryPerNation, controllingNation);
        }

        public override Military AcceptableMilitary => Military.Army;
    }
}
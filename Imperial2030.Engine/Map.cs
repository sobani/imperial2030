﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Imperial2030.Engine
{
    public sealed class Map
    {
        private readonly Dictionary<Identity, HashSet<Identity>> _connectedRegions = new Dictionary<Identity, HashSet<Identity>>();
        
        public Map(params (Identity, Identity)[] regionPairs)
            : this(regionPairs.AsEnumerable())
        {
        }

        public Map(IEnumerable<(Identity, Identity)> regionPairs)
        {
            foreach (var (region1, region2) in regionPairs)
            {
                AddConnection(region1, region2);
                AddConnection(region2, region1);
            }
        }

        private void AddConnection(Identity from, Identity to)
        {
            if (_connectedRegions.TryGetValue(from, out var connections))
                connections.Add(to);
            else
                _connectedRegions[from] = new HashSet<Identity> {to};
        }

        public IReadOnlyCollection<Identity> Regions => _connectedRegions.Keys;

        [Pure]
        public bool AreConnected(Identity region1, Identity region2)
        {
            return _connectedRegions.TryGetValue(region1, out var connections)
                && connections.Contains(region2);
        }

        [Pure]
        public IReadOnlyCollection<Identity> GetConnections(Identity region)
        {
            return _connectedRegions[region];
        }
    }
}
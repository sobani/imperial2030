﻿using System;

namespace Imperial2030.Engine
{
    public sealed class Identity : IEquatable<Identity>, IComparable<Identity>
    {
        public Identity(string name)
        {
            Name = name;
        }

        public string Name { get; }

        public override string ToString() => Name;

        public int CompareTo(Identity other)
        {
            return string.Compare(Name, other?.Name, StringComparison.Ordinal);
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as Identity);
        }

        public bool Equals(Identity? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        
        public static bool operator ==(Identity? left, Identity? right)
        {
            if (ReferenceEquals(left, null))
                return ReferenceEquals(right, null);
            
            return left.Equals(right);
        }

        public static bool operator !=(Identity? left, Identity? right)
        {
            return !(left == right);
        }
    }
}
﻿using System.Collections.Immutable;

namespace Imperial2030.Engine.Configuration
{
    public static class Constants
    {
        public static int MaxNationPower = 25;
        
        public static readonly Identity AnyIdentity = new Identity("");
        
        public static readonly Identity Russia = new Identity("Russia");
        public static readonly Identity China = new Identity("China");
        public static readonly Identity India = new Identity("India");
        public static readonly Identity Brazil = new Identity("Brazil");
        public static readonly Identity Usa = new Identity("USA");
        public static readonly Identity Europe = new Identity("Europe");
        
        // oceans
        public static readonly Identity Npa = new Identity("North Pacific");
        public static readonly Identity Spa = new Identity("South Pacific");
        public static readonly Identity Nat = new Identity("North Atlantic");
        public static readonly Identity Car = new Identity("Caribbean Sea");
        public static readonly Identity Sat = new Identity("South Atlantic");
        public static readonly Identity Gog = new Identity("Gulf of Guinea");
        public static readonly Identity Med = new Identity("Mediterranean Sea");
        public static readonly Identity Ioc = new Identity("Indian Ocean");
        public static readonly Identity Soj = new Identity("Sea of Japan");
        public static readonly Identity Chs = new Identity("China Sea");
        public static readonly Identity Tas = new Identity("Tasman Sea");
        
        // North America
        public static readonly Identity Ala = new Identity("Alaska");
        public static readonly Identity Can = new Identity("Canada");
        public static readonly Identity Que = new Identity("Quebec");
        public static readonly Identity Sfr = new Identity("San Francisco");
        public static readonly Identity Chc = new Identity("Chicago");
        public static readonly Identity Nyo = new Identity("New York");
        public static readonly Identity Orl = new Identity("New Orleans");
        public static readonly Identity Mex = new Identity("Mexico");
        
        // South America
        public static readonly Identity Col = new Identity("Colombia");
        public static readonly Identity Per = new Identity("Peru");
        public static readonly Identity Arg = new Identity("Argentina");
        public static readonly Identity Man = new Identity("Manaus");
        public static readonly Identity For = new Identity("Fortaleza");
        public static readonly Identity Bra = new Identity("Brasilia");
        public static readonly Identity Rio = new Identity("Rio de Janeiro");
        
        // Europe
        public static readonly Identity Lon = new Identity("London");
        public static readonly Identity Par = new Identity("Paris");
        public static readonly Identity Ber = new Identity("Berlin");
        public static readonly Identity Rom = new Identity("Rome");
        
        // Africa
        public static readonly Identity Naf = new Identity("North-Africa");
        public static readonly Identity Gui = new Identity("Guinea");
        public static readonly Identity Nig = new Identity("Nigeria");
        public static readonly Identity Eaf = new Identity("East-Africa");
        public static readonly Identity Con = new Identity("Congo");
        public static readonly Identity Saf = new Identity("South-Africa");
        
        // Soviet
        public static readonly Identity Mur = new Identity("Murmansk");
        public static readonly Identity Mos = new Identity("Moscow");
        public static readonly Identity Nov = new Identity("Novosibirsk");
        public static readonly Identity Vla = new Identity("Vladivostok");
        public static readonly Identity Ukr = new Identity("Ukraine");
        public static readonly Identity Kaz = new Identity("Kazakhstan");
        public static readonly Identity Mon = new Identity("Mongolia");
        
        // ME + India
        public static readonly Identity Tur = new Identity("Turkey");
        public static readonly Identity Nea = new Identity("Near East");
        public static readonly Identity Ira = new Identity("Iran");
        public static readonly Identity Afg = new Identity("Afghanistan");
        public static readonly Identity Del = new Identity("New Delhi");
        public static readonly Identity Mum = new Identity("Mumbai");
        public static readonly Identity Kol = new Identity("Kolkota");
        public static readonly Identity Che = new Identity("Chennai");
        
        // East Asia
        public static readonly Identity Uru = new Identity("Urumqi");
        public static readonly Identity Bej = new Identity("Beijing");
        public static readonly Identity Cho = new Identity("Chongqing");
        public static readonly Identity Sha = new Identity("Shanghai");
        public static readonly Identity Kor = new Identity("Korea");
        public static readonly Identity Jap = new Identity("Japan");
        public static readonly Identity Ich = new Identity("Indochina");
        
        // Oceania
        public static readonly Identity Phi = new Identity("Philippines");
        public static readonly Identity Ind = new Identity("Indonesia");
        public static readonly Identity Aus = new Identity("Australia");
        public static readonly Identity Zea = new Identity("New Zealand");
        
        public static readonly Map DefaultMap = new Map(
            // oceans
            (Npa, Spa), (Npa, Car), (Nat, Car), (Spa, Sat), (Car, Sat), (Nat, Gog), (Car, Gog), (Sat, Gog), (Nat, Med), (Gog, Ioc), (Sat, Ioc), (Med, Ioc), (Ioc, Chs), (Ioc, Tas), (Soj, Chs), (Chs, Tas), (Soj, Npa), (Chs, Npa), (Chs, Spa), (Tas, Spa),
            // North America
            (Ala, Can), (Can, Que), (Can, Sfr), (Can, Chc), (Can, Nyo), (Que, Nyo), (Sfr, Chc), (Chc, Nyo), (Sfr, Orl), (Chc, Orl), (Nyo, Orl), (Sfr, Mex), (Orl, Mex),
            // oceans - North America
            (Npa, Ala), (Npa, Can), (Npa, Sfr), (Npa, Mex), (Nat, Can), (Nat, Que), (Nat, Nyo), (Car, Orl), (Car, Mex),
            // South America
            (Col, Per), (Col, Man), (Per, Man), (Per, Bra), (Per, Arg), (Arg, Bra), (Arg, Rio), (Man, For), (Man, Bra), (For, Bra), (For, Rio), (Bra, Rio),
            // oceans - South America
            (Npa, Col), (Spa, Per), (Spa, Arg), (Car, Col), (Car, Man), (Car, For), (Sat, Rio), (Sat, Arg),
            // Europe
            (Lon, Par), (Par, Ber), (Par, Rom), (Ber, Rom),
            // oceans - Europe
            (Nat, Lon), (Nat, Par), (Nat, Ber), (Med, Par), (Med, Rom),
            // Africa
            (Naf, Gui), (Naf, Nig), (Naf, Eaf), (Gui, Nig), (Nig, Eaf), (Nig, Con), (Eaf, Con), (Eaf, Saf), (Con, Saf),
            // oceans - Africa
            (Nat, Naf), (Nat, Gui), (Gog, Gui), (Gog, Nig), (Gog, Con), (Gog, Saf), (Med, Naf), (Ioc, Naf), (Ioc, Eaf), (Ioc, Saf),
            // Soviet
            (Mur, Mos), (Mur, Nov), (Mur, Ukr), (Mos, Nov), (Mos, Ukr), (Mos, Kaz), (Nov, Vla), (Nov, Kaz), (Nov, Mon), (Vla, Mon), (Kaz, Mon),
            // oceans - Soviet
            (Nat, Mur), (Soj, Vla),
            // ME + India
            (Tur, Nea), (Tur, Ira), (Nea, Ira), (Ira, Afg), (Ira, Mum), (Afg, Del), (Afg, Mum), (Del, Mum), (Del, Kol), (Mum, Kol), (Mum, Che), (Kol, Che),
            // oceans - ME + India
            (Med, Tur), (Med, Nea), (Ioc, Nea), (Ioc, Ira), (Ioc, Mum), (Ioc, Che), (Ioc, Kol),
            // East Asia
            (Uru, Bej), (Uru, Cho), (Bej, Cho), (Bej, Sha), (Bej, Kor), (Cho, Sha), (Cho, Ich), (Sha, Ich),
            // oceans - East Asia
            (Soj, Kor), (Soj, Jap), (Chs, Bej), (Chs, Sha), (Chs, Kor), (Chs, Ich), (Ioc, Ich),
            // Oceania
            // oceans - Oceania
            (Ioc, Ind), (Chs, Phi), (Chs, Ind), (Tas, Ind), (Tas, Aus), (Tas, Zea),
            
            // intercontinental
            (Mex, Col), (Ber, Mur), (Ber, Ukr), (Rom, Ukr), (Rom, Tur), (Mos, Tur), (Mos, Ira), (Kaz, Afg), (Naf, Nea), (Vla, Bej), (Vla, Kor), (Kaz, Uru), (Afg, Uru), (Mon, Uru), (Mon, Bej), (Del, Uru), (Kol, Uru), (Kol, Cho), (Kol, Ich));

        public static readonly World DefaultWorld = new World(
            DefaultMap,
            new FleetProvince(Mur, Russia, false),
            new ArmyProvince(Mos, Russia, true),
            new ArmyProvince(Nov, Russia, false),
            new FleetProvince(Vla, Russia, true),
            new ArmyProvince(Uru, China, false),
            new ArmyProvince(Bej, China, true),
            new ArmyProvince(Cho, China, false),
            new FleetProvince(Sha, China, true),
            new FleetProvince(Mum, India, true),
            new ArmyProvince(Del, India, true),
            new ArmyProvince(Che, India, false),
            new FleetProvince(Kol, India, false),
            new ArmyProvince(Man, Brazil, false),
            new FleetProvince(For, Brazil, false),
            new ArmyProvince(Bra, Brazil, true),
            new FleetProvince(Rio, Brazil, true),
            new FleetProvince(Sfr, Usa, false),
            new ArmyProvince(Chc, Usa, true),
            new FleetProvince(Nyo, Usa, false),
            new FleetProvince(Orl, Usa, true),
            new FleetProvince(Lon, Europe, true),
            new ArmyProvince(Par, Europe, true),
            new ArmyProvince(Ber, Europe, false),
            new FleetProvince(Rom, Europe, false),
            new LandRegion(Ala),
            new LandRegion(Can),
            new LandRegion(Que),
            new LandRegion(Mex),
            new LandRegion(Col),
            new LandRegion(Per),
            new LandRegion(Arg),
            new LandRegion(Naf),
            new LandRegion(Gui),
            new LandRegion(Nig),
            new LandRegion(Eaf),
            new LandRegion(Con),
            new LandRegion(Saf),
            new LandRegion(Ukr),
            new LandRegion(Kaz),
            new LandRegion(Mon),
            new LandRegion(Tur),
            new LandRegion(Nea),
            new LandRegion(Ira),
            new LandRegion(Afg),
            new LandRegion(Kor),
            new LandRegion(Jap),
            new LandRegion(Ich),
            new LandRegion(Phi),
            new LandRegion(Ind),
            new LandRegion(Aus),
            new LandRegion(Zea),
            new SeaRegion(Npa),
            new SeaRegion(Spa),
            new SeaRegion(Nat),
            new SeaRegion(Car),
            new SeaRegion(Sat),
            new SeaRegion(Gog),
            new SeaRegion(Med),
            new SeaRegion(Ioc),
            new SeaRegion(Soj),
            new SeaRegion(Chs),
            new SeaRegion(Tas));
        
        public static readonly ImmutableList<Identity> DefaultNations = ImmutableList.Create(Russia, China, India, Brazil, Usa, Europe);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using MoreLinq.Extensions;

namespace Imperial2030.Engine.Configuration
{
    public class BondsGameStart : IGameStart
    {
        public IChoice CreateGame(IEnumerable<Identity> playerIdentities)
        {
            var playerIdentityList = playerIdentities.ToList();
            var money = GetMoneyPerPlayer(playerIdentityList.Count);
            var players = playerIdentityList.Select(id => new PlayerState(id, money)).Shuffle().ToImmutableList();

            var game = new Game(Constants.DefaultWorld, Constants.DefaultNations, players);
            game = WithInitialPlayerBonds(game);

            return game.FirstChoice();
        }

        private static int GetMoneyPerPlayer(int playerCount)
        {
            return playerCount switch
            {
                2 => 35,
                3 => 24,
                _ => 13
            };
        }

        private static Game WithInitialPlayerBonds(Game game)
        {
            var playerIdentities = game.Players.Select(p => p.Identity).Shuffle().ToList();
            
            var bondDistribution = GetBondDistribution(playerIdentities.Count);

            return bondDistribution
                .Aggregate(
                    game,
                    (g, tuple) =>
                    {
                        var player = playerIdentities[tuple.playerIndex];
                        var bond = g.GetNation(tuple.nation).Bonds[tuple.bondIndex];
                        return g.TryInvest(player, tuple.nation, bond)
                            .GetValueOrThrow($"{player} is unable to invest in {bond}");
                    });
        }

        private static List<(int playerIndex, Identity nation, int bondIndex)> GetBondDistribution(
            int playerCount)
        {
            if (playerCount == 2)
            {
                return new List<(int playerIndex, Identity nation, int bondIndex)>
                {
                    (0, Constants.Russia, 3), // russia 9
                    (0, Constants.Europe, 0), // europe 2
                    (0, Constants.India, 3), // india 9
                    (0, Constants.Brazil, 0), // brazil 2
                    (0, Constants.Usa, 3), // usa 9
                    (0, Constants.Russia, 0), // russia 2

                    (1, Constants.China, 3), // china 9
                    (1, Constants.Usa, 0), // usa 2
                    (1, Constants.Brazil, 2), // brazil 9
                    (1, Constants.China, 0), // china 2
                    (1, Constants.Europe, 2), // europe 9
                    (1, Constants.India, 0), // india 2
                };
            }
            
            if (playerCount == 3)
            {
                return new List<(int playerIndex, Identity nation, int bondIndex)>
                {
                    (0, Constants.Usa, 3), // usa 9
                    (0, Constants.Russia, 0), // russia 2
                    (0, Constants.India, 3), // india 9
                    (0, Constants.Brazil, 0), // brazil 2

                    (1, Constants.Brazil, 2), // brazil 9
                    (1, Constants.China, 0), // china 2
                    (1, Constants.Russia, 2), // russia 9
                    (1, Constants.Europe, 0), // europe 2

                    (2, Constants.Europe, 2), // europe 9
                    (2, Constants.India, 0), // india 2
                    (2, Constants.China, 2), // china 9
                    (2, Constants.Usa, 0) // usa 2
                };
            }
            
            var x = new List<(int playerIndex, Identity nation, int bondIndex)>
            {
                (0, Constants.Russia, 3), // russia 9
                (0, Constants.Europe, 0), // europe 2
                (1, Constants.China, 3), // china 9
                (1, Constants.Usa, 0), // usa 2
                (0, Constants.India, 3), // india 9
                (0, Constants.Brazil, 0), // brazil 2
                (1, Constants.Brazil, 2), // brazil 9
                (1, Constants.China, 0), // china 2
                (0, Constants.Usa, 2), // usa 9
                (0, Constants.Russia, 0), // russia 2
                (1, Constants.Europe, 2), // europe 9
                (1, Constants.India, 0), // india 2
            };
            
            throw new NotImplementedException("can't provide 4/5/6 players with bonds yet.");
        }
    }
}
﻿using System.Collections.Generic;
using Imperial2030.Engine.Choice;

namespace Imperial2030.Engine.Configuration
{
    public interface IGameStart
    {
        IChoice CreateGame(IEnumerable<Identity> playerIdentities);
    }
}
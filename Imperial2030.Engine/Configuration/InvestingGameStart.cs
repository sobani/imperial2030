﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice;
using MoreLinq.Extensions;

namespace Imperial2030.Engine.Configuration
{
    public class InvestingGameStart : IGameStart
    {
        public IChoice CreateGame(IEnumerable<Identity> playerIdentities)
        {
            var playerIdentityList = playerIdentities.ToList();
            var money = GetMoneyPerPlayer(playerIdentityList.Count);
            var players = playerIdentityList.Select(id => new PlayerState(id, money)).Shuffle().ToImmutableList();

            var game = new Game(Constants.DefaultWorld, Constants.DefaultNations, players);
            
            return game.InvestingStartChoice();
        }

        private static int GetMoneyPerPlayer(int playerCount)
        {
            return playerCount switch
            {
                2 => 37,
                3 => 25,
                4 => 19,
                5 => 15,
                _ => 13
            };
        }
    }
}
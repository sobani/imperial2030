﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class FleetProvince : Region
    {
        public FleetProvince(Identity identity, Identity nation, bool hasFactory)
            : this(identity, nation, ImmutableDictionary<Identity, MilitaryCollection>.Empty, hasFactory)
        {
        }

        public FleetProvince(
            Identity identity,
            Identity nation,
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            bool hasFactory)
            : base(identity, militaryPerNation, null)
        {
            HasFactory = hasFactory;
            Nation = nation;
        }

        [Pure]
        protected override Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
        {
            return new FleetProvince(Identity, Nation, militaryPerNation, HasFactory);
        }

        public bool HasFactory { get; }

        public Identity Nation { get; }

        public override int TaxFor(Identity nation)
        {
            return nation == Nation && HasFactory && !HasOpposingMilitary(nation)
                ? 2
                : 0;
        }

        public override Maybe<Region> WithProduce(Identity nation)
        {
            if (HasFactory && nation == Nation)
                return AddMilitary(Nation, Military.Fleet);
            
            return default;
        }

        [Pure]
        public override IEnumerable<(Military, Region newRegion)> ImportPossibilities(Identity nation)
        {
            if (nation != Nation)
                yield break;
            
            yield return (Military.Army, AddMilitary(nation, Military.Army));
            yield return (Military.Fleet, AddMilitary(nation, Military.Fleet));
        }

        [Pure]
        public override Military? TryGetFactory()
        {
            return HasFactory ? Military.Fleet : (Military?) null;
        }

        [Pure]
        public override Maybe<Region> WithFactory(Identity nation)
        {
            if (!HasFactory && nation == Nation)
                return new FleetProvince(Identity, Nation, MilitaryPerNation, true);

            return default;
        }

        public override Maybe<Region> DestroyFactory(Identity nation)
        {
            if (!HasFactory || nation == Nation)
                return default;

            if (MilitaryOf(nation).Count < 3 || !MilitaryOf(Nation).IsEmpty)
                return default;

            return new FleetProvince(Identity, Nation, MilitaryPerNation, false)
                .MoveMilitaryOut(nation, Military.Army)
                .Select(nr => nr.MoveMilitaryOut(nation, Military.Army))
                .Select(nr => nr.MoveMilitaryOut(nation, Military.Army));
        }
        
        // even though this province can _produce_ fleets, it will never accept any back
        public override Military AcceptableMilitary => Military.Army;

        public override bool HasRail(Identity nation)
        {
            return nation == Nation && !HasOpposingMilitary(nation);
        }
    }
}
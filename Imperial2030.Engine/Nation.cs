﻿using System;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using Imperial2030.Engine.Configuration;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class Nation
    {
        private static readonly ImmutableList<(int value, int interest)> StartingBonds = ImmutableList.Create(
            (2, 1),
            (4, 2),
            (6, 3),
            (9, 4),
            (12, 5),
            (16, 6),
            (20, 7),
            (25, 8),
            (30, 9));

        private Nation(Identity identity)
        {
            Identity = identity;
            Money = 0;
            Power = 0;
            Bonds = StartingBonds.ConvertAll(bond => new Bond(identity, bond.value, bond.interest));
            LastRondelActionIndex = -1;
        }
        
        private Nation(Identity identity, int money, int power, ImmutableList<Bond> bonds, int lastRondelActionIndex)
        {
            Identity = identity;
            Money = money;
            Power = power;
            Bonds = bonds;
            LastRondelActionIndex = lastRondelActionIndex;
        }

        [Pure]
        public static Nation New(Identity identity) => new Nation(identity);

        public Identity Identity { get; }
        
        public int Money { get; }
        
        public int Power { get; }

        public int PowerFactor => Power/5;
        
        public ImmutableList<Bond> Bonds { get; }
        
        public int LastRondelActionIndex { get; }

        [Pure]
        public Maybe<(Nation, int successBonus)> WithTax(Tax tax)
        {
            var newMoneyExclSuccessBonus = Money + tax.Revenue - tax.MilitaryCost;
            
            int successBonus;
            int newMoney;
            if (newMoneyExclSuccessBonus < 0)
            {
                successBonus = 0;
                newMoney = 0;
            }
            else
            {
                successBonus = Math.Min(newMoneyExclSuccessBonus, tax.SuccessBonus);
                newMoney = newMoneyExclSuccessBonus - successBonus;
            }
            
            var newPower = Math.Min(Constants.MaxNationPower, Power + tax.PowerIncrease);

            if (newMoney == Money && successBonus == 0 && newPower == Power)
                return default;

            return (new Nation(Identity, newMoney, newPower, Bonds, LastRondelActionIndex), successBonus);
        }

        [Pure]
        public Maybe<Nation> WithMoney(int newMoney)
        {
            if (newMoney == Money || newMoney < 0)
                return default;

            return new Nation(Identity, newMoney, Power, Bonds, LastRondelActionIndex);
        }

        [Pure]
        public Maybe<Nation> PayForFactory()
        {
            const int factoryCosts = 5;
            return WithMoney(Money - factoryCosts);
        }

        [Pure]
        public Maybe<Nation> SellBond(Bond bond)
        {
            var newBonds = Bonds.Remove(bond);
            if (newBonds == Bonds)
                return default;

            var newMoney = Money + bond.Value;
            return new Nation(Identity, newMoney, Power, newBonds, LastRondelActionIndex);
        }

        [Pure]
        public Maybe<Nation> SellBond(Bond bond, Bond tradeIn)
        {
            if (bond.Value <= tradeIn.Value)
                return default;

            var bondIndex = Bonds.IndexOf(bond);
            if (bondIndex == -1)
                return default;

            var newBonds = Bonds.SetItem(bondIndex, tradeIn).Sort();
            var newMoney = Money + bond.Value - tradeIn.Value;
            return new Nation(Identity, newMoney, Power, newBonds, LastRondelActionIndex);
        }

        public Nation WithLastRondelActionIndex(int index)
        {
            return new Nation(Identity, Money, Power, Bonds, index);
        }
    }
}
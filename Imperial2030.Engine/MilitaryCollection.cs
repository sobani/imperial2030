﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class MilitaryCollection : IReadOnlyCollection<Military>
    {
        public static readonly MilitaryCollection Empty = new MilitaryCollection(ImmutableList<Military>.Empty);
        
        private readonly ImmutableList<Military> _list;

        public MilitaryCollection(params Military[] military)
        {
            _list = ImmutableList.Create(military).Sort();
        }

        public MilitaryCollection(
            IEnumerable<Military> list)
        {
            _list = list.ToImmutableList();
        }

        public MilitaryCollection(
            ImmutableList<Military> list)
        {
            _list = list;
        }

        public bool IsEmpty => _list.IsEmpty;

        public int Count => _list.Count;

        public IEnumerable<Military> Types => _list.Distinct();

        [Pure]
        public bool Contains(Military military)
        {
            return _list.Contains(military);
        }

        public int CountOf(Military military)
        {
            return _list.Count(m => m == military);
        }

        [Pure]
        public MilitaryCollection Add(Military military)
        {
            return new MilitaryCollection(
                military == Military.Army
                    ? _list.Insert(0, Military.Army)
                    : _list.Add(Military.Fleet));
        }

        [Pure]
        public Maybe<MilitaryCollection> TryRemove(Military military)
        {
            return _list.TryRemove(military)
                .Select(newList => new MilitaryCollection(newList));
        }

        public IEnumerator<Military> GetEnumerator() => _list.GetEnumerator();

        [Pure]
        public override string ToString()
        {
            return string.Join("", _list.Select(military => military.ToString()[0]));
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static implicit operator MilitaryCollection(Military military)
        {
            return new MilitaryCollection(ImmutableList.Create(military));
        }
    }
}
﻿using System;

namespace Imperial2030.Engine
{
    public sealed class Bond : IComparable<Bond>
    {
        public Bond(Identity nation, int value, int interest)
        {
            Nation = nation;
            Value = value;
            Interest = interest;
        }

        public Identity Nation { get; }
        public int Value { get; }
        public int Interest { get; }

        public int CompareTo(Bond? other)
        {
            if (ReferenceEquals(this, other))
                return 0;
            if (ReferenceEquals(null, other))
                return 1;
            return Interest.CompareTo(other.Interest);
        }

        public override string ToString()
        {
            return $"{Nation} bond {Value} ({Interest})";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public abstract class Region
    {
        protected Region(
            Identity identity)
        {
            Identity = identity;
            MilitaryPerNation = ImmutableDictionary<Identity, MilitaryCollection>.Empty;
        }

        protected Region(
            Identity identity,
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation)
        {
            Identity = identity;
            MilitaryPerNation = militaryPerNation;
            ControllingNation = controllingNation;
        }

        private Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> newMilitaryPerNation)
        {
            var newControllingNation = DetermineNewControllingNation(newMilitaryPerNation);

            return NewRegion(newMilitaryPerNation, newControllingNation);
        }

        [Pure]
        protected abstract Region NewRegion(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity? controllingNation);

        public Identity Identity { get; }

        public Identity? ControllingNation { get; }

        protected ImmutableDictionary<Identity, MilitaryCollection> MilitaryPerNation { get; }

        public virtual int TaxFor(Identity nation) => ControllingNation == nation ? 1 : 0;

        [Pure]
        public virtual Maybe<Region> WithProduce(Identity nation) => default;

        [Pure]
        public virtual IEnumerable<(Military, Region newRegion)> ImportPossibilities(Identity nation) => Enumerable.Empty<(Military, Region)>();

        [Pure]
        public virtual Military? TryGetFactory() => null;

        [Pure]
        public virtual Maybe<Region> WithFactory(Identity nation) => default;
        
        [Pure]
        public virtual Maybe<Region> DestroyFactory(Identity nation) => default;

        /// <summary>
        /// Returns the <see cref="Military"/> that can be <see cref="MoveMilitaryIn"/>.
        /// </summary>
        public abstract Military AcceptableMilitary { get; }

        public IEnumerable<Identity> NationsWithMilitary => MilitaryPerNation.Keys;

        public bool CanHaveBattle => MilitaryPerNation.Count > 1;

        [Pure]
        public MilitaryCollection MilitaryOf(Identity nationIdentity)
        {
            return MilitaryPerNation.TryGetValue(nationIdentity, out var military)
                ? military
                : MilitaryCollection.Empty;
        }

        [Pure]
        public virtual bool HasRail(Identity nation) => false;

        [Pure]
        public Maybe<Region> MoveMilitaryOut(Identity nation, Military military)
        {
            var newMilitaryPerNation = Remove(MilitaryPerNation, nation, military);

            return newMilitaryPerNation.Select(NewRegion);
        }

        [Pure]
        public Region MoveMilitaryIn(
            Identity nation,
            Military military)
        {
            if (military != AcceptableMilitary)
                throw new ArgumentException(Identity + " does not accept military type " + military, nameof(military));

            return AddMilitary(nation, AcceptableMilitary);
        }

        [Pure]
        public Maybe<Region> Battle(
            Identity activatorNation,
            Military activatorMilitary,
            Identity targetNation,
            Military targetMilitary)
        {
            return Remove(MilitaryPerNation, activatorNation, activatorMilitary)
                .Select(mpn => Remove(mpn, targetNation, targetMilitary))
                .Select(NewRegion);
        }

        [Pure]
        protected Region AddMilitary(Identity nation, Military military)
        {
            var newMilitaryPerNation = MilitaryPerNation
                .SetItem(nation, MilitaryOf(nation).Add(military));

            return NewRegion(newMilitaryPerNation);
        }

        private Maybe<ImmutableDictionary<Identity, MilitaryCollection>> Remove(
            ImmutableDictionary<Identity, MilitaryCollection> militaryPerNation,
            Identity nation,
            Military military)
        {
            if (!militaryPerNation.TryGetValue(nation, out var oldMilitary))
                return default;

            return oldMilitary.TryRemove(military)
                .Select(
                    newMilitary => newMilitary.IsEmpty
                        ? militaryPerNation.Remove(nation)
                        : militaryPerNation.SetItem(nation, newMilitary));
        }

        private Identity? DetermineNewControllingNation(
            ImmutableDictionary<Identity, MilitaryCollection> newMilitaryPerNation)
        {
            return newMilitaryPerNation.Count == 1
                ? newMilitaryPerNation.Keys.Single()
                : ControllingNation;
        }

        protected bool HasOpposingMilitary(Identity nation) => MilitaryPerNation.Count switch
        {
            0 => false,
            1 => !MilitaryPerNation.ContainsKey(nation),
            _ => true
        };
    }
}
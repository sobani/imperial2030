﻿namespace Imperial2030.Engine.Choice
{
    public interface INationChoice : IChoice
    {
        Identity ActiveNationIdentity { get; }
        Identity DecisionNationIdentity { get; }
    }
}
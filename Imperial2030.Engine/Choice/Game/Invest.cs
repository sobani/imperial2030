﻿using System;
using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Game
{
    public class Invest : IChoice
    {
        private readonly Identity _playerIdentity;
        private readonly Identity _nationIdentity;
        private readonly Func<Engine.Game, CancelUndo, IChoice> _nextChoiceFactory;
        private readonly CancelUndo _cancelUndo;

        public Invest(
            Engine.Game game,
            Identity playerIdentity,
            Identity nationIdentity,
            Func<Engine.Game, CancelUndo, IChoice> nextChoiceFactory,
            CancelUndo cancelUndo)
        {
            _playerIdentity = playerIdentity;
            _nationIdentity = nationIdentity;
            _nextChoiceFactory = nextChoiceFactory;
            _cancelUndo = cancelUndo;
            Game = game;
        }
        
        public Engine.Game Game { get; }
        public Identity DecisionPlayerIdentity => _playerIdentity;

        public string Title => "Invest in " + _nationIdentity;

        public bool AreOptionsPublic => false;
        
        public IEnumerable<IOption> Options
        {
            get
            {
                var noneOption = new LazyOption("None", game => _nextChoiceFactory(game, CancelUndo.Undo(this)), Game);

                return BuyOptions.Append(noneOption).Concat(_cancelUndo.Options);
            }
        }

        private IEnumerable<IOption> BuyOptions
        {
            get
            {
                var player = Game.GetPlayer(_playerIdentity);
                var nation = Game.GetNation(_nationIdentity);
                
                foreach (var nationBond in nation.Bonds)
                foreach (var newGame in Game.TryInvest(player.Identity, nation.Identity, nationBond))
                {
                    yield return new LazyOption(
                        $"buy {ToString(nationBond)}",
                        game => _nextChoiceFactory(game, CancelUndo.Undo(this)),
                        newGame);
                }

                foreach (var playerBond in player.Bonds.Where(bond => bond.Nation == nation.Identity))
                {
                    foreach (var nationBond in nation.Bonds)
                    foreach (var newGame in Game.TryInvest(player.Identity, nation.Identity, nationBond, playerBond))
                    {
                        yield return new LazyOption(
                            $"upgrade {ToString(playerBond)} to {ToString(nationBond)}",
                            game => _nextChoiceFactory(game, CancelUndo.Undo(this)),
                            newGame);
                    }
                }
            }
        }

        private static string ToString(Bond bond)
        {
            return $"{bond.Value} ({bond.Interest})";
        }
    }
}
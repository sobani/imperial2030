﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Game
{
    public class Tax : INationChoice
    {
        private readonly CancelUndo _cancelUndo;

        public Tax(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            CancelUndo cancelUndo)
        {
            _cancelUndo = cancelUndo;
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
        }

        public Engine.Game Game { get; }
        public Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public Identity ActivePlayerIdentity { get; }
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;
        public string Title
        {
            get
            {
                var tax = Engine.Tax.Calculate(Game.World, ActiveNationIdentity);
                return $"revenue: {tax.Revenue}, soldiers' pay: {tax.MilitaryCost}, success bonus: {tax.SuccessBonus}, power increase: {tax.PowerIncrease}";
            }
        }

        public IEnumerable<IOption> Options
        {
            get
            {
                var taxOptions = Game.WithTax(ActiveNationIdentity)
                    .Select(newGame => new Option.Option("Tax", newGame.NextChoice(CancelUndo.Undo(this))))
                    .AsEnumerable();

                return taxOptions
                    .Concat(_cancelUndo.Options);
            }
        }
    }
}
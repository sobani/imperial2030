﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Game
{
    public class Investor : INationChoice
    {
        private readonly CancelUndo _cancelUndo;

        public Investor(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            CancelUndo cancelUndo)
        {
            _cancelUndo = cancelUndo;
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
        }

        public Engine.Game Game { get; }
        public Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public Identity ActivePlayerIdentity { get; }
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;

        public string Title
        {
            get
            {
                var investor = Engine.Investor.Calculate(Game, ActiveNationIdentity);
                var playerDisplayValues =
                    investor.PlayerInterest.ToImmutableDictionary(kvp => kvp.Key, kvp => kvp.Value.ToString());
                
                if (investor.MissingMoney > 0)
                {
                    var controllingPlayerIdentity = Game.GetControllingPlayerIdentityOrThrow(ActiveNationIdentity);
                    var controllingPlayerInterest = investor.PlayerInterest[controllingPlayerIdentity];
                    var controllingPlayerMoneyChange = controllingPlayerInterest - investor.MissingMoney;
                    var controllingPlayerValue = $"{controllingPlayerMoneyChange} ({controllingPlayerInterest} - {investor.MissingMoney})";

                    playerDisplayValues = playerDisplayValues.SetItem(
                        controllingPlayerIdentity,
                        controllingPlayerValue);
                }

                return "Investor - " + string.Join(", ", playerDisplayValues.Select(kvp => kvp.Key + ": " + kvp.Value));
            }
        }
        
        public IEnumerable<IOption> Options
        {
            get
            {
                var investor = Engine.Investor.Calculate(Game, ActiveNationIdentity);
                var distributeOptions = Game.WithInvestor(ActiveNationIdentity, investor)
                    .Select(newGame => new Option.Option("Distribute", newGame.NextChoice(CancelUndo.Undo(this))))
                    .AsEnumerable();

                return distributeOptions
                    .Concat(_cancelUndo.Options);
            }
        }
    }
}
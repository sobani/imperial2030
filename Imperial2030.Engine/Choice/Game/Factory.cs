﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Game
{
    public class Factory : INationChoice
    {
        private readonly CancelUndo _cancelUndo;

        public Factory(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            CancelUndo cancelUndo)
        {
            _cancelUndo = cancelUndo;
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
        }

        public Engine.Game Game { get; }
        public Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public Identity ActivePlayerIdentity { get; }
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;
        public string Title => "Select the region to build a factory";

        public IEnumerable<IOption> Options
        {
            get
            {
                var buildOptions =
                    Game.World.Regions.Values
                        .OrderBy(region => region.Identity)
                        .SelectMany(GetRegionOptions);
                
                return buildOptions.Concat(_cancelUndo.Options);
            }
        }

        private IEnumerable<SelectRegionOption> GetRegionOptions(Region region)
        {
            return Game.WithFactory(region.Identity, ActiveNationIdentity)
                .Select(newGame => new SelectRegionOption(region.Identity, newGame.NextChoice(CancelUndo.Undo(this))))
                .AsEnumerable();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Game
{
    public class Import : INationChoice
    {
        private const int ImportsPerTurn = 3;
        
        private readonly int _importsLeft;
        private readonly CancelUndo _cancelUndo;

        private Import(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            int importsLeft,
            CancelUndo cancelUndo)
        {
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
            _importsLeft = importsLeft;
            _cancelUndo = cancelUndo;
        }

        public Import(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            CancelUndo cancelUndo)
        {
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
            _importsLeft = ImportsPerTurn;
            _cancelUndo = cancelUndo;
        }

        public Engine.Game Game { get; }
        public Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public Identity ActivePlayerIdentity { get; }
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;
        public string Title => $"Import military ({_importsLeft} left)";

        public IEnumerable<IOption> Options
        {
            get
            {
                if (Game.GetNation(ActiveNationIdentity).Money > 0)
                {
                    var importOptions = Game.World.Regions.Values
                        .OrderBy(region => region.Identity)
                        .SelectMany(region => region.ImportPossibilities(ActiveNationIdentity))
                        .Select(GetImportOption)
                        .Values();

                    foreach (var option in importOptions)
                        yield return option;
                }

                foreach (var option in _cancelUndo.Options)
                    yield return option;

                if (_importsLeft < ImportsPerTurn)
                    yield return new Option.Option("Done", Game.NextChoice(CancelUndo.Undo(this)));
            }
        }

        private Maybe<ImportOption> GetImportOption((Military military, Region newRegion) import)
        {
            var (military, newRegion) = import;
            var nation = Game.GetNation(ActiveNationIdentity);

            return Game
                .WithRegion(newRegion)
                .Select(newGame => newGame.WithNationMoney(ActiveNationIdentity, nation.Money - 1))
                .Select(
                    newGame =>
                    {
                        var nextChoice = _importsLeft == 1
                            ? newGame.NextChoice(CancelUndo.Undo(this))
                            : new Import(newGame, ActiveNationIdentity, ActivePlayerIdentity, _importsLeft - 1, _cancelUndo.WithUndo(this));

                        return new ImportOption(newRegion.Identity, military, nextChoice);

                    });
        }
    }
}
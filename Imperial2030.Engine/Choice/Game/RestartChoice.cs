﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Configuration;
using MoreLinq;

namespace Imperial2030.Engine.Choice.Game
{
    public class RestartChoice : IChoice
    {
        public RestartChoice(
            Engine.Game game)
        {
            Game = game;
        }
        
        public Engine.Game Game { get; }
        public Identity DecisionPlayerIdentity => Constants.AnyIdentity;
        public string Title => $"{WinningPlayer.Name} won with {WinningPlayer.Score} points!";
        
        private PlayerInfo WinningPlayer => Game.Players.MaxBy(p => p.Score).First();

        private IEnumerable<Identity> PlayerIdentities => Game.Players.Select(player => player.Identity);

        public IEnumerable<IOption> Options
        {
            get
            {
                var gameStart = new InvestingGameStart();
                var nextChoice = gameStart.CreateGame(PlayerIdentities);

                yield return new Option.Option("Restart", nextChoice);
            }
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Game
{
    public class Gift : INationChoice
    {
        private readonly RondelAction _rondel;
        private readonly CancelUndo _cancelUndo;

        public Gift(
            RondelAction rondel,
            CancelUndo cancelUndo)
        {
            _rondel = rondel;
            _cancelUndo = cancelUndo;
            Game = rondel.Game;
        }

        public Gift(
            RondelAction rondel,
            Engine.Game game,
            CancelUndo cancelUndo)
        {
            _rondel = rondel;
            Game = game;
            _cancelUndo = cancelUndo;
        }

        public Engine.Game Game { get; }
        public Identity DecisionPlayerIdentity => _rondel.ActivePlayerIdentity;

        public Identity ActiveNationIdentity => _rondel.ActiveNationIdentity;
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public string Title => "Gift";

        public bool AreOptionsPublic => false;

        public IEnumerable<IOption> Options =>
            GiftOptions
                .Concat(DoneOption.AsEnumerable())
                .Concat(_cancelUndo.Options);

        public IEnumerable<Option.Option> GiftOptions =>
            from controllingPlayer in Game.TryGetControllingPlayer(ActiveNationIdentity).AsEnumerable()
            from newGame in Game.WithGift(controllingPlayer, ActiveNationIdentity, 1)
            select new Option.Option("1", new Gift(_rondel, newGame, CancelUndo.Undo(this)));

        private Maybe<Option.Option> DoneOption =>
            from newRondel in _rondel.WithGame(Game)
            select new Option.Option("Done", newRondel);
    }
}
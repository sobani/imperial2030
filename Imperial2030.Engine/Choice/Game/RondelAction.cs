﻿using System;
using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Game
{
    public sealed class RondelAction : INationChoice
    {
        private readonly CancelUndo _cancelUndo;

        public RondelAction(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity activePlayerIdentity,
            CancelUndo cancelUndo)
        {
            _cancelUndo = cancelUndo;
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = activePlayerIdentity;
        }

        public Engine.Game Game { get; }
        public  Identity ActiveNationIdentity { get; }
        public Identity ActivePlayerIdentity { get; }

        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;

        public string Title => "Choose next Rondel action.";

        public IEnumerable<IOption> Options
        {
            get
            {
                const int totalOptionCount = 8;
                
                var lastRondelActionIndex = Game.GetNation(ActiveNationIdentity).LastRondelActionIndex;
                if (lastRondelActionIndex == -1)
                {
                    var allOptions = Enumerable.Range(0, totalOptionCount)
                        .Select(i => GetOption(i, 0))
                        .Values();

                    return allOptions
                        .Concat(_cancelUndo.Options)
                        .Concat(GiftOption.AsEnumerable());
                }

                var freeOptions = Enumerable.Range(1, 3)
                    .Select(i => GetOption((lastRondelActionIndex + i)%totalOptionCount, 0))
                    .Values();

                int extraCost = Game.GetNation(ActiveNationIdentity).PowerFactor + 1;
                var paidOptions = Enumerable.Range(1, 3)
                    .Select(i => GetOption((lastRondelActionIndex + i + 3)%totalOptionCount, i*extraCost))
                    .Values();
                
                return freeOptions
                    .Concat(paidOptions)
                    .Concat(_cancelUndo.Options)
                    .Concat(GiftOption.AsEnumerable());
            }
        }
        
        private Maybe<IOption> GetOption(int index, int cost)
        {
            return Game
                .WithNation(Game.GetNation(ActiveNationIdentity).WithLastRondelActionIndex(index))
                .Select(game => game.WithPlayerCost(ActivePlayerIdentity, cost))
                .Select(game => GetOption(game, index, cost));
        }

        private Maybe<IOption> GetOption(Engine.Game game, int index, int cost)
        {
            var titleCost = cost > 0 ? $" (${cost})" : "";
            var cancelUndo = CancelUndo.Cancel(this);

            switch (index)
            {
                case 0:
                    return new Option.Option("Tax" + titleCost, new Tax(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                case 1:
                    return new Option.Option("Factory" + titleCost, new Factory(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                case 2:
                case 6:
                    return new Option.Option("Production" + titleCost, new Produce(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                case 3:
                case 7:
                    return new Option.Option("Maneuver" + titleCost, new Maneuver.Maneuver(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                case 4:
                    return new Option.Option("Investor" + titleCost, new Investor(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                case 5:
                    return new Option.Option("Import" + titleCost, new Import(game, ActiveNationIdentity, ActivePlayerIdentity, cancelUndo));
                default:
                    throw new ArgumentOutOfRangeException(nameof(index), index, "index can't be " + index);
            }
        }

        public Maybe<IOption> GiftOption =>
            GiftChoice.GiftOptions.Any()
                ? new Option.Option("Gift", GiftChoice)
                : default(Maybe<IOption>);

        public Gift GiftChoice => new Gift(this, CancelUndo.Cancel(this));

        public Maybe<RondelAction> WithGame(Engine.Game game)
        {
            if (game == Game)
                return default;
            
            return new RondelAction(game, ActiveNationIdentity, ActivePlayerIdentity, _cancelUndo);
        }
    }
}
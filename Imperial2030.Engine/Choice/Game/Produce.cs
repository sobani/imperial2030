﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Game
{
    public class Produce : INationChoice
    {
        private readonly CancelUndo _cancelUndo;
        private readonly ImmutableList<Region> _produceRegions;

        public Produce(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity decisionPlayerIdentity,
            CancelUndo cancelUndo)
        {
            _produceRegions = GetInitProduceRegions(game.World, activeNationIdentity);
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = decisionPlayerIdentity;
            _cancelUndo = cancelUndo;
        }

        private static ImmutableList<Region> GetInitProduceRegions(World world, Identity activeNation)
        {
            var builder = ImmutableList.CreateBuilder<Region>();
            foreach (var region in world.Regions.Values.OrderBy(region => region.Identity))
            {
                region.WithProduce(activeNation)
                    .Do(builder.Add);
            }

            return builder.ToImmutable();
        }

        public Produce(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity decisionPlayerIdentity,
            ImmutableList<Region> produceRegions,
            CancelUndo cancelUndo)
        {
            _produceRegions = produceRegions;
            _cancelUndo = cancelUndo;
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            ActivePlayerIdentity = decisionPlayerIdentity;
        }

        public Engine.Game Game { get; }
        public  Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;
        public Identity ActivePlayerIdentity { get; }
        Identity IChoice.DecisionPlayerIdentity => ActivePlayerIdentity;
        public string Title => "Produce military";

        public IEnumerable<IOption> Options =>
            GetProduceAllOption()
                .Concat(_produceRegions.Select(GetProduceOption))
                .Concat(_cancelUndo.Options)
                .Append(new Option.Option("Done", Game.NextChoice(CancelUndo.Undo(this))));

        private IEnumerable<IOption> GetProduceAllOption()
        {
            if (_produceRegions.Count <= 1)
                yield break;
            
            var newWorld = _produceRegions.Aggregate(
                Game.World,
                (current, region) => current.WithRegion(region)
                    .GetValueOrThrow("GetProduceAllOption couldn't replace region"));
            var newGame = Game.WithWorld(newWorld).GetValueOrThrow("GetProduceAllOption couldn't replace world");
            yield return new Option.Option("All " + _produceRegions.Count, newGame.NextChoice(CancelUndo.Undo(this)));
        }

        private SelectRegionOption GetProduceOption(Region region)
        {
            var newGame = Game.WithRegion(region).GetValueOrThrow("GetProduceAllOption couldn't replace region");
            var nextChoice = _produceRegions.Count > 1
                ? new Produce(newGame, ActiveNationIdentity, ActivePlayerIdentity, _produceRegions.Remove(region), _cancelUndo.WithUndo(this))
                : newGame.NextChoice(CancelUndo.Undo(this));
            
            return new SelectRegionOption(region.Identity, nextChoice);
        }
    }
}
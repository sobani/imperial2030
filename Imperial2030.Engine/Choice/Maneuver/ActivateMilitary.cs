﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class ActivateMilitary : INationChoice
    {
        private readonly Maneuver _maneuver;
        private readonly Region _region;

        public ActivateMilitary(
            Maneuver maneuver,
            Region region,
            Military activeNationMilitary)
        {
            _maneuver = maneuver;
            _region = region;
            ActiveNationMilitary = activeNationMilitary;
        }

        public Military ActiveNationMilitary { get; }
        public Engine.Game Game => _maneuver.Game;
        public Identity DecisionPlayerIdentity => _maneuver.DecisionPlayerIdentity;
        public Identity ActiveNationIdentity => _maneuver.ActiveNationIdentity;
        Identity INationChoice.DecisionNationIdentity => _maneuver.ActiveNationIdentity;

        public string Title => "What do you want to do?";

        public IEnumerable<IOption> Options
        {
            get
            {
                // var destroyedFactoryRegion = _region.DestroyFactory(ActiveNationIdentity);
                // if (destroyedFactoryRegion != _region)
                // {
                    // yield return new Option.Option("Destroy Factory", _maneuver.WithRegion(destroyedFactoryRegion));
                // }
                
                foreach (var option in BattleOptions)
                    yield return option;

                yield return new CancelOption(_maneuver);
            }
        }

        private IEnumerable<IOption> BattleOptions =>
            GetInactiveNationIdentities()
                .OrderBy(nation => nation.Name)
                .SelectMany(BattleOptionsAgainstOpponent);

        private IEnumerable<IOption> BattleOptionsAgainstOpponent(Identity opponent)
        {
            var militaryTypes = _region.MilitaryOf(opponent).Types.ToList();
            if (militaryTypes.Count == 1)
            {
                return NextChoiceAfterBattle(opponent, militaryTypes.Single())
                    .Select(nextChoice => BattleOption.WithoutMilitaryChoice(opponent, nextChoice))
                    .AsEnumerable();
            }

            return militaryTypes.Select(
                    military => NextChoiceAfterBattle(opponent, military)
                        .Select(nextChoice => BattleOption.WithOpponentMilitaryChoice(opponent, military, nextChoice)))
                .Values();
        }

        [Pure]
        private Maybe<Maneuver> NextChoiceAfterBattle(Identity inactiveNationIdentity, Military inactiveNationMilitary)
        {
            return _maneuver.WithBattleWithoutMove(
                _region.Identity,
                ActiveNationMilitary,
                inactiveNationIdentity,
                inactiveNationMilitary,
                CancelUndo.Undo(this));
        }
        
        private IEnumerable<Identity> GetInactiveNationIdentities() =>
            _region.NationsWithMilitary.Except(new[] {_maneuver.ActiveNationIdentity});
    }
}
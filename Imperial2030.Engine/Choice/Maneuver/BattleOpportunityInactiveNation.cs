﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class BattleOpportunityInactiveNation : INationChoice
    {
        private readonly Maneuver _maneuver;
        private readonly Region _region;
        private readonly Military _activeNationMilitary;
        private readonly Identity _inactiveNationIdentity;
        private readonly ImmutableList<Identity> _remainingInactiveNationIdentities;
        private readonly CancelUndo _cancelUndo;

        public BattleOpportunityInactiveNation(
            Maneuver maneuver,
            Region region,
            Military activeNationMilitary,
            Identity inactiveNationIdentity,
            ImmutableList<Identity> remainingInactiveNationIdentities,
            CancelUndo cancelUndo)
        {
            _maneuver = maneuver;
            _region = region;
            _activeNationMilitary = activeNationMilitary;
            _inactiveNationIdentity = inactiveNationIdentity;
            _remainingInactiveNationIdentities = remainingInactiveNationIdentities;
            _cancelUndo = cancelUndo;
        }

        public Engine.Game Game => _maneuver.Game;
        public Identity DecisionPlayerIdentity => Game.GetControllingPlayerIdentityOrThrow(_inactiveNationIdentity);
        public Identity ActiveNationIdentity => _maneuver.ActiveNationIdentity;
        Identity INationChoice.DecisionNationIdentity => _inactiveNationIdentity;

        public string Title => $"Do you want to battle in {_region.Identity.Name}?";

        public IEnumerable<IOption> Options =>
            BattleOptions
                .Append(new NoBattleOption(NextChoiceAfterNoBattle()))
                .Concat(_cancelUndo.Options);

        private IEnumerable<IOption> BattleOptions
        {
            get
            {
                var militaryTypes = _region.MilitaryOf(_inactiveNationIdentity).Types.ToList();
                if (militaryTypes.Count == 1)
                {
                    return
                        from militaryType in militaryTypes
                        from nextChoice in NextChoiceAfterBattle(militaryType)
                        select BattleOption.WithoutMilitaryChoice(ActiveNationIdentity, nextChoice);
                }
                else
                {
                    return
                        from militaryType in militaryTypes
                        from nextChoice in NextChoiceAfterBattle(militaryType)
                        select BattleOption.WithMilitaryChoice(ActiveNationIdentity, militaryType, nextChoice);
                }
            }
        }

        [Pure]
        private Maybe<Maneuver> NextChoiceAfterBattle(Military inactiveNationMilitary)
        {
            return _maneuver.WithBattleAfterMove(
                _region.Identity,
                _activeNationMilitary,
                _inactiveNationIdentity,
                inactiveNationMilitary,
                CancelUndo.Undo(this));
        }

        [Pure]
        private IChoice NextChoiceAfterNoBattle()
        {
            switch (_remainingInactiveNationIdentities.Count)
            {
                case 0:
                    return _maneuver.WithCancelUndo(CancelUndo.Undo(this));
                case 1:
                    return new BattleOpportunityInactiveNation(
                        _maneuver,
                        _region,
                        _activeNationMilitary,
                        _remainingInactiveNationIdentities.Single(),
                        ImmutableList<Identity>.Empty,
                        CancelUndo.Undo(this));
                default:
                    return new BattleOpportunityDetermineInactiveNation(
                        _maneuver,
                        _region,
                        _activeNationMilitary,
                        _remainingInactiveNationIdentities,
                        CancelUndo.Undo(this));
            }
        }
    }
}
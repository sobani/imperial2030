﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class RegionManeuver : INationChoice
    {
        private readonly Maneuver _maneuver;
        private readonly Identity _currentRegionIdentity;
        private readonly Military _military;

        public RegionManeuver(
            Maneuver maneuver,
            Identity currentRegionIdentity,
            Military military)
        {
            _maneuver = maneuver;
            _currentRegionIdentity = currentRegionIdentity;
            _military = military;
        }

        public Engine.Game Game => _maneuver.Game;
        public Identity DecisionPlayerIdentity => _maneuver.DecisionPlayerIdentity;
        public Identity ActiveNationIdentity => _maneuver.ActiveNationIdentity;
        private Region CurrentRegion => _maneuver.Game.World.Regions[_currentRegionIdentity];
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;

        private IEnumerable<Identity> AvailableRailRegions
        {
            get
            {
                if (_military != Military.Army)
                    return Enumerable.Empty<Identity>();
                
                var currentRegion = Game.World.Regions[_currentRegionIdentity];
                if (!currentRegion.HasRail(ActiveNationIdentity))
                    return Enumerable.Empty<Identity>();
                
                var availableMilitaryForMove = _maneuver.AvailableForMove(_currentRegionIdentity, _military, true);
                if (availableMilitaryForMove.IsEmpty)
                    return Enumerable.Empty<Identity>();

                return Game.World.Map
                    .GetConnections(_currentRegionIdentity)
                    .Where(region =>
                    {
                        var regionTo = Game.World.Regions[region];
                        return regionTo.HasRail(ActiveNationIdentity) && availableMilitaryForMove.Contains(regionTo.AcceptableMilitary);
                    });
            }
        }

        private IEnumerable<Identity> AvailableNeighbourRegions
        {
            get
            {
                var availableMilitaryForMove = _maneuver.AvailableForMove(_currentRegionIdentity, _military, false);
                if (availableMilitaryForMove.IsEmpty)
                    return Enumerable.Empty<Identity>();
                
                return Game.World.Map
                    .GetConnections(_currentRegionIdentity)
                    .Where(region => availableMilitaryForMove.Contains(Game.World.Regions[region].AcceptableMilitary));
            }
        }

        private IEnumerable<Identity> RegionsAccessibleByConvoy
        {
            get
            {
                if (_military != Military.Army)
                    return Enumerable.Empty<Identity>();
                
                Func<Region, bool> IsConnectedWith(Identity regionIdentity)
                {
                    return otherRegion => Game.World.Map.AreConnected(otherRegion.Identity, regionIdentity);
                }
                
                // implicit: except where the fleet is in harbor (FleetProvince)
                var regionsWithFleet = Game.World.Regions.Values
                    .OfType<SeaRegion>()
                    .Where(region => region.MilitaryOf(ActiveNationIdentity).Contains(Military.Fleet))
                    .ToList();

                var fleetRegionsAccessibleByConvoy = regionsWithFleet
                    .Where(IsConnectedWith(_currentRegionIdentity))
                    .ToList();

                bool hasAddedRegion;
                do
                {
                    hasAddedRegion = false;
                    foreach (var region in regionsWithFleet.Except(fleetRegionsAccessibleByConvoy))
                    {
                        if (fleetRegionsAccessibleByConvoy.Any(IsConnectedWith(region.Identity)))
                        {
                            fleetRegionsAccessibleByConvoy.Add(region);
                            hasAddedRegion = true;
                        }
                    }
                } while (hasAddedRegion);

                return Game.World.Regions.Values
                    .Where(region => region.AcceptableMilitary == Military.Army)
                    .Select(region => region.Identity)
                    .Where(regionIdentity => fleetRegionsAccessibleByConvoy.Any(IsConnectedWith(regionIdentity)))
                    .Except(new[] {_currentRegionIdentity});
            }
        }

        public string Title => $"Moving military from {_currentRegionIdentity.Name}, select a destination:";

        public IEnumerable<IOption> Options =>
            RailMoveOptions
                .Concat(NeighbourMoveOptions)
                .Concat(ConvoyMoveOptions)
                .Concat(ActivateOptions)
                .Concat(DestroyOptions)
                .Append(new CancelOption(_maneuver));

        private IEnumerable<IOption> RailMoveOptions =>
            from region in AvailableRailRegions
            orderby region.Name
            from choice in MoveToByRail(region)
            select new RegionOption(region.Name + " (rail)", region, choice);


        private IEnumerable<IOption> NeighbourMoveOptions =>
            from region in AvailableNeighbourRegions.Except(AvailableRailRegions)
            orderby region.Name
            from choice in MoveTo(region)
            select new SelectRegionOption(region, choice);

        private IEnumerable<IOption> ConvoyMoveOptions
        {
            get
            {
                if (_military != Military.Army)
                    return Enumerable.Empty<IOption>();
                
                var availableMilitaryForMove = _maneuver.AvailableForMove(_currentRegionIdentity, Military.Army, false);
                
                if (availableMilitaryForMove.IsEmpty)
                    return Enumerable.Empty<IOption>();

                return
                    from region in RegionsAccessibleByConvoy.Except(AvailableNeighbourRegions)
                    orderby region.Name
                    from choice in ConvoyTo(region)
                    select new RegionOption(region.Name + " (convoy)", region, choice);
            }
        }

        private IEnumerable<IOption> ActivateOptions
        {
            get
            {
                var activateChoice = new ActivateMilitary(_maneuver, CurrentRegion, _military);
                if (activateChoice.Options.All(option => option is CancelOption))
                    return Enumerable.Empty<IOption>();

                return new IOption[] {new ActivateOption(activateChoice)};
            }
        }

        private IEnumerable<IOption> DestroyOptions =>
            _maneuver
                .WithDestroyFactory(CurrentRegion.Identity)
                .Select(nextChoice => new Option.Option("Destroy Factory", nextChoice))
                .AsEnumerable();

        [Pure]
        private Maybe<Maneuver> MoveToByRail(
            Identity regionTo)
        {
            var military = Game.World.Regions[regionTo].AcceptableMilitary;

            return _maneuver.WithMoveByRail(_currentRegionIdentity, regionTo, military);
        }

        [Pure]
        private Maybe<IChoice> MoveTo(
            Identity regionTo)
        {
            return _maneuver
                .WithMove(_currentRegionIdentity, regionTo, _military)
                .Select(
                    newManeuver =>
                    {
                        var newRegionTo = newManeuver.Game.World.Regions[regionTo];

                        if (!newRegionTo.CanHaveBattle)
                            return (IChoice) newManeuver;

                        return new BattleOpportunityActiveNation(newManeuver, newRegionTo, _military, CancelUndo.Cancel(_maneuver));
                    });
        }

        [Pure]
        private Maybe<IChoice> ConvoyTo(
            Identity regionTo)
        {
            // todo remove used fleets from consideration
            // todo select convoy

            return MoveTo(regionTo);
        }
        
        IEnumerable<IOption> IChoice.Options => Options;
    }
}
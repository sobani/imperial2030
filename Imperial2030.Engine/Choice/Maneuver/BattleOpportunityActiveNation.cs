﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class BattleOpportunityActiveNation : INationChoice
    {
        private readonly Maneuver _maneuver;
        private readonly Region _region;
        private readonly Military _activeNationMilitary;
        private readonly CancelUndo _cancelUndo;

        public BattleOpportunityActiveNation(
            Maneuver maneuver,
            Region region,
            Military activeNationMilitary,
            CancelUndo cancelUndo)
        {
            if (!region.CanHaveBattle)
                throw new ArgumentException("can't battle in region " + region, nameof(region));

            _maneuver = maneuver;
            _region = region;
            _activeNationMilitary = activeNationMilitary;
            _cancelUndo = cancelUndo;
        }

        public Engine.Game Game => _maneuver.Game;
        public Identity DecisionPlayerIdentity => _maneuver.DecisionPlayerIdentity;
        public Identity ActiveNationIdentity => _maneuver.ActiveNationIdentity;
        Identity INationChoice.DecisionNationIdentity => _maneuver.ActiveNationIdentity;

        public string Title => $"Do you want to battle in {_region.Identity.Name}?";

        public IEnumerable<IOption> Options
        {
            get
            {
                return GetInactiveNationIdentities()
                    .OrderBy(nation => nation.Name)
                    .SelectMany(OptionsForOpponent)
                    .Append(new NoBattleOption(NextChoiceAfterNoBattle()))
                    .Concat(_cancelUndo.Options);
            }
        }

        private IEnumerable<IOption> OptionsForOpponent(Identity opponent)
        {
            var militaryTypes = _region.MilitaryOf(opponent).Types.ToList();
            if (militaryTypes.Count == 1)
            {
                return NextChoiceAfterBattle(opponent, militaryTypes.Single())
                    .Select(nc => BattleOption.WithoutMilitaryChoice(opponent, nc))
                    .AsEnumerable();
            }

            return militaryTypes.Select(
                    military => NextChoiceAfterBattle(opponent, military)
                        .Select(nc => BattleOption.WithOpponentMilitaryChoice(opponent, military, nc)))
                .Values();
        }

        [Pure]
        private Maybe<Maneuver> NextChoiceAfterBattle(Identity inactiveNation, Military inactiveNationMilitary)
        {
            return _maneuver.WithBattleAfterMove(
                _region.Identity,
                _activeNationMilitary,
                inactiveNation,
                inactiveNationMilitary,
                CancelUndo.Undo(this));
        }

        private IChoice NextChoiceAfterNoBattle()
        {
            var inactiveNationIdentities = GetInactiveNationIdentities();
            switch (inactiveNationIdentities.Count)
            {
                case 1:
                    return new BattleOpportunityInactiveNation(
                        _maneuver.WithCancelUndo(CancelUndo.Undo(this)),
                        _region,
                        _activeNationMilitary,
                        inactiveNationIdentities.Single(),
                        ImmutableList<Identity>.Empty,
                        CancelUndo.Undo(this));
                default:
                    return new BattleOpportunityDetermineInactiveNation(
                        _maneuver.WithCancelUndo(CancelUndo.Undo(this)),
                        _region,
                        _activeNationMilitary,
                        inactiveNationIdentities,
                        _cancelUndo.WithUndo(this));
            }
        }

        private ImmutableList<Identity> GetInactiveNationIdentities() =>
            ImmutableList.CreateRange(
                    _region.NationsWithMilitary.Except(new[] {_maneuver.ActiveNationIdentity}))
                .Sort();
    }
}
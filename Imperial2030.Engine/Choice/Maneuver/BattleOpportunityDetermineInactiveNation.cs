﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class BattleOpportunityDetermineInactiveNation : INationChoice
    {
        private readonly Maneuver _maneuver;
        private readonly Region _region;
        private readonly Military _activeNationMilitary;
        private readonly CancelUndo _cancelUndo;

        public BattleOpportunityDetermineInactiveNation(
            Maneuver maneuver,
            Region region,
            Military activeNationMilitary,
            ImmutableList<Identity> inactiveNationIdentities,
            CancelUndo cancelUndo)
        {
            if (inactiveNationIdentities.Count <= 1)
                throw new ArgumentException("must have at least 2 opponents", nameof(inactiveNationIdentities));
            
            _maneuver = maneuver;
            _region = region;
            _activeNationMilitary = activeNationMilitary;
            _cancelUndo = cancelUndo;
            InactiveNationIdentities = inactiveNationIdentities;
        }

        public Engine.Game Game => _maneuver.Game;
        public Identity DecisionPlayerIdentity => _maneuver.DecisionPlayerIdentity;
        Identity INationChoice.ActiveNationIdentity => _maneuver.ActiveNationIdentity;
        Identity INationChoice.DecisionNationIdentity => _maneuver.ActiveNationIdentity;
        public ImmutableList<Identity> InactiveNationIdentities { get; }

        public string Title => $"Who gets to decide if there is a battle in {_region.Identity.Name} first?";

        public IEnumerable<IOption> Options => NationOptions.Concat(_cancelUndo.Options);

        public IEnumerable<SelectNationOption> NationOptions =>
            InactiveNationIdentities.Select(GetOption);

        private SelectNationOption GetOption(Identity opponent)
        {
            var choiceForOpponent = new BattleOpportunityInactiveNation(
                _maneuver,
                _region,
                _activeNationMilitary,
                opponent,
                InactiveNationIdentities.Remove(opponent),
                CancelUndo.Undo(this));
            
            return new SelectNationOption(opponent, choiceForOpponent);
        }
    }
}
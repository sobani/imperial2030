﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class ManeuverMilitaryCollection
    {
        public static ManeuverMilitaryCollection Empty { get; } = new ManeuverMilitaryCollection(ImmutableList<(Military, MilitaryState)>.Empty);
        
        private readonly ImmutableList<(Military military, MilitaryState state)> _militaryStates;

        private ManeuverMilitaryCollection(ImmutableList<(Military, MilitaryState)> militaryStates)
        {
            _militaryStates = militaryStates;
        }
        
        [Pure]
        public static ManeuverMilitaryCollection New(MilitaryCollection military)
        {
            var states = military.Select(m => (m, MilitaryState.Idle)).ToImmutableList();
            if (states.IsEmpty)
                return Empty;
            
            return new ManeuverMilitaryCollection(states);
        }

        public bool IsEmpty => _militaryStates.IsEmpty;

        public IEnumerable<Military> AvailableForMove(bool hasRail)
        {
            var militaryStates =
                hasRail
                    ? _militaryStates
                    : _militaryStates.Where(tuple => tuple.state == MilitaryState.Idle);

            return militaryStates.Select(tuple => tuple.military);
        }

        public IEnumerable<Military> AvailableForConvoy => _militaryStates
            .Where(tuple => tuple.state != MilitaryState.IsConvoy)
            .Select(tuple => tuple.military);

        [Pure]
        public (ManeuverMilitaryCollection, MilitaryState) RemoveMovableByRail(Military military)
        {
            var newStates = _militaryStates.Remove((military, MilitaryState.Idle));
            if (newStates != _militaryStates)
                return (Wrap(newStates), MilitaryState.Idle);

            return (Wrap(_militaryStates.Remove((military, MilitaryState.HasMoved))), MilitaryState.HasMoved);
        }

        [Pure]
        public ManeuverMilitaryCollection RemoveMovable(Military military)
        {
            return Wrap(
                _militaryStates.Remove((military, MilitaryState.Idle)));
        }

        [Pure]
        public ManeuverMilitaryCollection RemoveFleetOptions()
        {
            var newCollection = ReplaceState(Military.Fleet, MilitaryState.Idle, MilitaryState.HasMoved);

            if (newCollection == this)
                return this;
            
            return newCollection
                .RemoveFleetOptions();
        }

        [Pure]
        public ManeuverMilitaryCollection AddMovedByRail(Military military, MilitaryState previousState)
        {
            return new ManeuverMilitaryCollection(
                _militaryStates.Add((military, previousState)));
        }

        [Pure]
        public ManeuverMilitaryCollection AddMoved(Military military)
        {
            return new ManeuverMilitaryCollection(
                _militaryStates.Add((military, MilitaryState.HasMoved)));
        }

        [Pure]
        public ManeuverMilitaryCollection MarkConvoy()
        {
            var newCollection = ReplaceState(
                Military.Fleet,
                MilitaryState.HasMoved,
                MilitaryState.IsConvoy);
            
            if (newCollection != this)
                return newCollection;

            return ReplaceState(
                Military.Fleet,
                MilitaryState.Idle,
                MilitaryState.IsConvoy);
        }

        [Pure]
        public ManeuverMilitaryCollection RemoveBattleAfterMove(Military military)
        {
            return Wrap(
                _militaryStates.Remove((military, MilitaryState.HasMoved)));
        }

        [Pure]
        public ManeuverMilitaryCollection RemoveBattleWithoutMove(Military military)
        {
            return Wrap(
                _militaryStates.Remove((military, MilitaryState.Idle)));
        }

        [Pure]
        public ManeuverMilitaryCollection RemoveForDestroyingFactory()
        {
            if (_militaryStates.Count(tuple => tuple.military == Military.Army) < 3)
                return this;

            return RemoveWorstState(Military.Army)
                .RemoveWorstState(Military.Army)
                .RemoveWorstState(Military.Army);
        }

        [Pure]
        private ManeuverMilitaryCollection ReplaceState(
            Military military,
            MilitaryState oldState,
            MilitaryState newState)
        {
            var index = _militaryStates.IndexOf((military, oldState));
            if (index == -1)
                return this;

            var newStates = _militaryStates.SetItem(index, (military, newState));

            return new ManeuverMilitaryCollection(newStates);
        }

        [Pure]
        private ManeuverMilitaryCollection RemoveWorstState(Military military)
        {
            ImmutableList<(Military military, MilitaryState state)> newStates = _militaryStates.Remove((military, MilitaryState.IsConvoy));
            if (newStates != _militaryStates)
                return Wrap(newStates);
            
            newStates = _militaryStates.Remove((military, MilitaryState.HasMoved));
            if (newStates != _militaryStates)
                return Wrap(newStates);

            newStates = _militaryStates.Remove((military, MilitaryState.Idle));
            return Wrap(newStates);
        }

        [Pure]
        private ManeuverMilitaryCollection Wrap(
            ImmutableList<(Military military, MilitaryState state)> newStates)
        {
            if (newStates == _militaryStates)
                return this;

            if (newStates.IsEmpty)
                return Empty;
            
            return new ManeuverMilitaryCollection(newStates);
        }
    }
}
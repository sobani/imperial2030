﻿using System.Collections.Generic;
using System.Linq;
using Imperial2030.Engine.Choice.Option;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class Maneuver : INationChoice
    {
        private readonly CancelUndo _cancelUndo;
        private readonly RegionMilitaryCollection _regionMilitaryCollection;

        public Maneuver(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity decisionPlayerIdentity,
            CancelUndo cancelUndo)
        {
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            DecisionPlayerIdentity = decisionPlayerIdentity;
            _cancelUndo = cancelUndo;
            _regionMilitaryCollection = RegionMilitaryCollection.Create(game.World, activeNationIdentity);
        }

        private Maneuver(
            Engine.Game game,
            Identity activeNationIdentity,
            Identity decisionPlayerIdentity,
            CancelUndo cancelUndo,
            RegionMilitaryCollection regionMilitaryCollection)
        {
            Game = game;
            ActiveNationIdentity = activeNationIdentity;
            DecisionPlayerIdentity = decisionPlayerIdentity;
            _cancelUndo = cancelUndo;
            _regionMilitaryCollection = regionMilitaryCollection;
        }

        public Engine.Game Game { get; }
        public Identity DecisionPlayerIdentity { get; }
        public Identity ActiveNationIdentity { get; }
        Identity INationChoice.DecisionNationIdentity => ActiveNationIdentity;

        public string Title => "Select a region to move military from.";

        public IEnumerable<IOption> Options
        {
            get
            {
                var armyOnlyManeuver = Wrap(_regionMilitaryCollection.RemoveFleetOptions());

                if (armyOnlyManeuver == this)
                {
                    foreach (var option in GetRegionOptions(Military.Army))
                        yield return option;
                }
                else
                {
                    foreach (var option in GetRegionOptions(Military.Fleet))
                        yield return option;

                    yield return new Option.Option("Move Army", armyOnlyManeuver);
                }

                foreach (var option in _cancelUndo.Options)
                {
                    yield return option;
                }

                yield return new Option.Option("Done", Game.NextChoice(CancelUndo.Undo(this)));
            }
        }

        private IEnumerable<IOption> GetRegionOptions(Military military)
        {
            return
                from regionFrom in _regionMilitaryCollection.Regions.OrderBy(region => region.Name)
                let nextChoice = new RegionManeuver(this, regionFrom, military)
                where !nextChoice.Options.All(option => option is CancelOption)
                select new SelectRegionOption(regionFrom, nextChoice);
        }

        public MilitaryCollection AvailableForMove(Identity region, Military requestedMilitary, bool hasRail)
        {
            return _regionMilitaryCollection.AvailableForMove(region, requestedMilitary, hasRail);
        }

        public Maybe<Maneuver> WithMoveByRail(Identity regionFrom, Identity regionTo, Military military)
        {
            var newCollection = _regionMilitaryCollection.WithMoveByRail(regionFrom, regionTo, military);
            var newWorld = Game.World.MoveMilitary(ActiveNationIdentity, regionFrom, regionTo, military);

            return Wrap(newWorld, newCollection);
        }

        public Maybe<Maneuver> WithMove(Identity regionFrom, Identity regionTo, Military military)
        {
            var newCollection = _regionMilitaryCollection.WithMove(regionFrom, regionTo, military);
            var newWorld = Game.World.MoveMilitary(ActiveNationIdentity, regionFrom, regionTo, military);

            return Wrap(newWorld, newCollection);
        }

        public Maybe<Maneuver> WithBattleAfterMove(Identity region, Military activeNationMilitary, Identity inactiveNation, Military inactiveNationMilitary, CancelUndo cancelUndo)
        {
            var newCollection = _regionMilitaryCollection.RemoveBattleAfterMove(region, activeNationMilitary);
            var newWorld = Game.World.Battle(region, ActiveNationIdentity, activeNationMilitary, inactiveNation, inactiveNationMilitary);

            return Wrap(newWorld, cancelUndo, newCollection);
        }

        public Maybe<Maneuver> WithBattleWithoutMove(Identity region, Military activeNationMilitary, Identity inactiveNation, Military inactiveNationMilitary, CancelUndo cancelUndo)
        {
            var newCollection = _regionMilitaryCollection.RemoveBattleWithoutMove(region, activeNationMilitary);
            var newWorld = Game.World.Battle(region, ActiveNationIdentity, activeNationMilitary, inactiveNation, inactiveNationMilitary);

            return Wrap(newWorld, cancelUndo, newCollection);
        }

        public Maybe<Maneuver> WithDestroyFactory(Identity region)
        {
            return Wrap(
                Game.World.DestroyFactory(region, ActiveNationIdentity),
                _regionMilitaryCollection.RemoveArmiesForDestroyingFactory(region));
        }

        public Maneuver WithCancelUndo(
            CancelUndo cancelUndo)
        {
            return new Maneuver(Game, ActiveNationIdentity, DecisionPlayerIdentity, cancelUndo, _regionMilitaryCollection);
        }

        private Maybe<Maneuver> Wrap(
            Maybe<World> newWorld,
            CancelUndo cancelUndo,
            RegionMilitaryCollection newCollection)
        {
            var newGame = newWorld.Select(Game.WithWorld);
            if (newCollection == _regionMilitaryCollection)
                return default;

            return newGame.Select(ng => new Maneuver(ng, ActiveNationIdentity, DecisionPlayerIdentity, cancelUndo, newCollection));
        }

        private Maybe<Maneuver> Wrap(
            Maybe<World> newWorld,
            RegionMilitaryCollection newCollection)
        {
            var newGame = newWorld.Select(Game.WithWorld);
            if (newCollection == _regionMilitaryCollection)
                return default;

            var cancelUndo = _cancelUndo.WithUndo(this);
            return newGame.Select(ng => new Maneuver(ng, ActiveNationIdentity, DecisionPlayerIdentity, cancelUndo, newCollection));
        }

        private Maneuver Wrap(
            RegionMilitaryCollection newCollection)
        {
            if (newCollection == _regionMilitaryCollection)
                return this;

            var cancelUndo = _cancelUndo.WithUndo(this);
            return new Maneuver(Game, ActiveNationIdentity, DecisionPlayerIdentity, cancelUndo, newCollection);
        }
    }
}
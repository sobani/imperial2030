﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;

namespace Imperial2030.Engine.Choice.Maneuver
{
    public sealed class RegionMilitaryCollection
    {
        public static RegionMilitaryCollection Empty { get; } = new RegionMilitaryCollection(ImmutableDictionary<Identity, ManeuverMilitaryCollection>.Empty);
        
        private readonly ImmutableDictionary<Identity, ManeuverMilitaryCollection> _militaryStatesPerRegion;
        
        private RegionMilitaryCollection(
            ImmutableDictionary<Identity, ManeuverMilitaryCollection> militaryStatesPerRegion)
        {
            _militaryStatesPerRegion = militaryStatesPerRegion;
        }

        [Pure]
        private static RegionMilitaryCollection New(
            ImmutableDictionary<Identity, ManeuverMilitaryCollection> militaryStatesPerRegion)
        {
            if (militaryStatesPerRegion.IsEmpty)
                return Empty;

            return new RegionMilitaryCollection(militaryStatesPerRegion);
        }
        
        [Pure]
        public static RegionMilitaryCollection Create(
            World initialWorld,
            Identity activeNation)
        {
            return New(
                InitialAvailableMilitaryPerRegion(initialWorld, activeNation));
        }

        private static ImmutableDictionary<Identity, ManeuverMilitaryCollection> InitialAvailableMilitaryPerRegion(
            World world,
            Identity activeNation)
        {
            return world.Regions.Values
                .Select(region => KeyValuePair.Create(region.Identity, ManeuverMilitaryCollection.New(region.MilitaryOf(activeNation))))
                .Where(kvp => !kvp.Value.IsEmpty)
                .ToImmutableDictionary();
        }

        public ManeuverMilitaryCollection this[Identity region] =>
            _militaryStatesPerRegion.TryGetValue(region, out var collection)
                ? collection
                : ManeuverMilitaryCollection.Empty;

        public IEnumerable<Identity> Regions => _militaryStatesPerRegion.Keys;

        public MilitaryCollection AvailableForMove(Identity region, Military requestedMilitary, bool hasRail)
        {
            return new MilitaryCollection(this[region].AvailableForMove(hasRail).Where(available => available == requestedMilitary));
        }

        [Pure]
        public RegionMilitaryCollection WithMoveByRail(Identity regionFrom, Identity regionTo, Military military)
        {
            var fromCollection = this[regionFrom];
            var (newFromCollection, previousState) = fromCollection.RemoveMovableByRail(military);
            if (newFromCollection == fromCollection)
                return this;

            var newToCollection = this[regionTo].AddMovedByRail(military, previousState);
            
            var newMilitaryStatesPerRegion = _militaryStatesPerRegion
                .SetItem(regionFrom, newFromCollection)
                .SetItem(regionTo, newToCollection);
            
            return new RegionMilitaryCollection(newMilitaryStatesPerRegion);
        }

        [Pure]
        public RegionMilitaryCollection WithMove(Identity regionFrom, Identity regionTo, Military military)
        {
            var fromCollection = this[regionFrom];
            var newFromCollection = fromCollection.RemoveMovable(military);
            if (newFromCollection == fromCollection)
                return this;

            var newToCollection = this[regionTo].AddMoved(military);
            
            var newMilitaryStatesPerRegion = _militaryStatesPerRegion
                .SetItem(regionFrom, newFromCollection)
                .SetItem(regionTo, newToCollection);
            
            return new RegionMilitaryCollection(newMilitaryStatesPerRegion);
        }

        [Pure]
        public RegionMilitaryCollection MarkConvoy(IEnumerable<Identity> convoyRegions)
        {
            var newStates = _militaryStatesPerRegion.ToBuilder();
            foreach (var region in convoyRegions)
            {
                var oldCollection = this[region];
                var newCollection = oldCollection.MarkConvoy();
                if (oldCollection == newCollection)
                    return this;
                
                newStates[region] = newCollection;
            }

            return Wrap(newStates.ToImmutable());
        }

        [Pure]
        public RegionMilitaryCollection RemoveBattleAfterMove(Identity region, Military military)
        {
            return Wrap(region, this[region].RemoveBattleAfterMove(military));
        }

        [Pure]
        public RegionMilitaryCollection RemoveBattleWithoutMove(Identity region, Military military)
        {
            return Wrap(region, this[region].RemoveBattleWithoutMove(military));
        }

        [Pure]
        public RegionMilitaryCollection RemoveArmiesForDestroyingFactory(Identity region)
        {
            return Wrap(region, this[region].RemoveForDestroyingFactory());
        }

        [Pure] public RegionMilitaryCollection RemoveFleetOptions()
        {
            var newMilitaryStates = _militaryStatesPerRegion.ToBuilder();
            foreach (var (region, maneuverMilitaryCollection) in _militaryStatesPerRegion)
            {
                var newCollection = maneuverMilitaryCollection.RemoveFleetOptions();

                if (newCollection != maneuverMilitaryCollection)
                    newMilitaryStates[region] = newCollection;
            }
            
            return Wrap(newMilitaryStates.ToImmutable());
        }

        [Pure]
        private RegionMilitaryCollection Wrap(Identity region, ManeuverMilitaryCollection newCollection)
        {
            var newStates = newCollection.IsEmpty
                ? _militaryStatesPerRegion.Remove(region)
                : _militaryStatesPerRegion.SetItem(region, newCollection);

            return Wrap(newStates);
        }

        [Pure]
        private RegionMilitaryCollection Wrap(ImmutableDictionary<Identity, ManeuverMilitaryCollection> newStates)
        {
            if (newStates == _militaryStatesPerRegion)
                return this;

            return New(newStates);
        }
    }
}
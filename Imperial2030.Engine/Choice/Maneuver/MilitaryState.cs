﻿namespace Imperial2030.Engine.Choice.Maneuver
{
    public enum MilitaryState
    {
        Idle,
        HasMoved,
        IsConvoy,
    }
}
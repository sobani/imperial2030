﻿using System.Collections.Generic;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice
{
    public interface IChoice
    {
        public Engine.Game Game { get; }
        public Identity DecisionPlayerIdentity { get; }
        public string Title { get; }
        public bool AreOptionsPublic => true;
        public IEnumerable<IOption> Options { get; }
    }
}
﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class CancelOption : IOption
    {
        public CancelOption(IChoice previousChoice)
        {
            PreviousChoice = previousChoice;
        }

        public string Title => "Cancel";
        public IChoice PreviousChoice { get; }
        
        IChoice IOption.NextChoice => PreviousChoice;
    }
}
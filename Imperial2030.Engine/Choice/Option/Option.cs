﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class Option : IOption
    {
        public Option(string title, IChoice nextChoice)
        {
            Title = title;
            NextChoice = nextChoice;
        }

        public string Title { get; }
        public IChoice NextChoice { get; }
    }
}
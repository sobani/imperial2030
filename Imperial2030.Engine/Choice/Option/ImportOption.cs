﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class ImportOption : RegionOption
    {
        public ImportOption(Identity region, Military military, IChoice nextChoice)
            : base($"{region} ({military})", region, nextChoice)
        {
            Military = military;
        }

        public Military Military { get; }
    }
}
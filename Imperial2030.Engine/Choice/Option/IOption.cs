﻿namespace Imperial2030.Engine.Choice.Option
{
    public interface IOption
    {
        string Title { get; }
        IChoice NextChoice { get; }
    }
}
﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class UndoOption : IOption
    {
        public UndoOption(IChoice previousChoice)
        {
            PreviousChoice = previousChoice;
        }

        public string Title => "Undo";
        public IChoice PreviousChoice { get; }
        
        IChoice IOption.NextChoice => PreviousChoice;
    }
}
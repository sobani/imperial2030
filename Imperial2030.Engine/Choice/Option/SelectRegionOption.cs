﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class SelectRegionOption : RegionOption
    {
        public SelectRegionOption(Identity region, IChoice nextChoice)
            : base(region.Name, region, nextChoice)
        {
        }
    }
}
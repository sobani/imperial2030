﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class NoBattleOption : IOption
    {
        public NoBattleOption(IChoice nextChoice)
        {
            NextChoice = nextChoice;
        }

        public string Title => "No Battle";
        public IChoice NextChoice { get; }
    }
}
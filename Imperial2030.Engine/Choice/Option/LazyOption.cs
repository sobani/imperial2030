﻿using System;

namespace Imperial2030.Engine.Choice.Option
{
    public sealed class LazyOption : IOption
    {
        private readonly Func<Engine.Game, IChoice> _nextChoiceFactory;
        private readonly Engine.Game _game;

        public LazyOption(string title, Func<Engine.Game, IChoice> nextChoiceFactory, Engine.Game game)
        {
            _nextChoiceFactory = nextChoiceFactory;
            _game = game;
            Title = title;
        }

        public string Title { get; }
        public IChoice NextChoice => _nextChoiceFactory(_game);
    }
}
﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class SelectNationOption : IOption
    {
        public SelectNationOption(Identity nation, IChoice nextChoice)
        {
            Nation = nation;
            NextChoice = nextChoice;
        }

        public string Title => Nation.Name;
        public Identity Nation { get; }
        public IChoice NextChoice { get; }
    }
}
﻿namespace Imperial2030.Engine.Choice.Option
{
    public sealed class BattleOption : IOption
    {
        private BattleOption(Identity nation, Maneuver.Maneuver nextChoice, string title)
        {
            Nation = nation;
            NextChoice = nextChoice;
            Title = title;
        }

        public static BattleOption WithoutMilitaryChoice(Identity nation, Maneuver.Maneuver nextChoice)
        {
            var title = $"Battle {nation.Name}";
            return new BattleOption(nation, nextChoice, title);
        }

        public static BattleOption WithMilitaryChoice(Identity nation, Military military, Maneuver.Maneuver nextChoice)
        {
            var title = $"Battle {nation.Name} with your {military.ToString()}";
            return new BattleOption(nation, nextChoice, title);
        }

        public static BattleOption WithOpponentMilitaryChoice(Identity nation, Military military, Maneuver.Maneuver nextChoice)
        {
            var title = $"Battle {nation.Name} ({military.ToString()})";
            return new BattleOption(nation, nextChoice, title);
        }

        public string Title { get; }
        public Identity Nation { get; }
        public Maneuver.Maneuver NextChoice { get; }
        
        IChoice IOption.NextChoice => NextChoice;
    }
}
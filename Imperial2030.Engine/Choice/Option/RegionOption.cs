﻿namespace Imperial2030.Engine.Choice.Option
{
    public class RegionOption : IOption
    {
        public RegionOption(string title, Identity region, IChoice nextChoice)
        {
            Title = title;
            Region = region;
            NextChoice = nextChoice;
        }
        
        public string Title { get; }
        public Identity Region { get; }
        public IChoice NextChoice { get; }
    }
}
﻿using Imperial2030.Engine.Choice.Maneuver;

namespace Imperial2030.Engine.Choice.Option
{
    public sealed class ActivateOption : IOption
    {
        public ActivateOption(ActivateMilitary nextChoice)
        {
            NextChoice = nextChoice;
        }

        public string Title => "Activate " + Military;
        public Military Military => NextChoice.ActiveNationMilitary;
        public ActivateMilitary NextChoice { get; }
        
        IChoice IOption.NextChoice => NextChoice;
    }
}
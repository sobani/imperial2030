﻿using System.Collections.Generic;
using Imperial2030.Engine.Choice.Option;

namespace Imperial2030.Engine.Choice
{
    public class CancelUndo
    {
        private readonly IChoice? _cancelChoice;
        private readonly IChoice? _undoChoice;

        private CancelUndo(IChoice? cancelChoice, IChoice? undoChoice)
        {
            _cancelChoice = cancelChoice;
            _undoChoice = undoChoice;
        }

        public static CancelUndo None => new CancelUndo(null, null);

        public static CancelUndo Both(IChoice cancelChoice, IChoice undoChoice)
        {
            return new CancelUndo(cancelChoice, undoChoice);
        }

        public static CancelUndo Cancel(IChoice cancelChoice)
        {
            return new CancelUndo(cancelChoice, null);
        }

        public static CancelUndo Undo(IChoice undoChoice)
        {
            return new CancelUndo(null, undoChoice);
        }

        public IEnumerable<IOption> Options
        {
            get
            {
                if (_cancelChoice != null)
                    yield return new CancelOption(_cancelChoice);
                if (_undoChoice != null)
                    yield return new UndoOption(_undoChoice);
            }
        }

        public CancelUndo WithUndo(IChoice undoChoice)
        {
            return new CancelUndo(_cancelChoice, undoChoice);
        }
    }
}
﻿using System.Linq;

namespace Imperial2030.Engine
{
    public sealed class Tax
    {
        private Tax(int revenue, int militaryCost)
        {
            Revenue = revenue;
            MilitaryCost = militaryCost;
        }

        public static Tax Calculate(World world, Identity nation)
        {
            var revenue = world.Regions.Values.Sum(region => region.TaxFor(nation));
            var militaryCost = world.Regions.Values.Sum(region => region.MilitaryOf(nation).Count);
            
            return new Tax(revenue, militaryCost);
        }

        public int Revenue { get; }
        
        public int MilitaryCost { get; }

        public int SuccessBonus => GetSuccessBonus(Revenue);

        public int PowerIncrease => GetPowerIncrease(Revenue);

        private static int GetSuccessBonus(int revenue)
        {
            if (revenue < 6)
                return 0;
            if (revenue < 10)
                return 1;
            
            if (revenue >= 18)
                return 5;
            
            return revenue/2 - 3;
        }

        private static int GetPowerIncrease(int revenue)
        {
            if (revenue < 6)
                return 0;
            if (revenue < 8)
                return 1;
            if (revenue < 10)
                return 2;
            
            if (revenue == 17)
                return 9;
            if (revenue >= 18)
                return 10;
            
            return revenue - 7;
        }
    }
}

﻿using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.Contracts;
using System.Linq;
using Imperial2030.Engine.Utils;

namespace Imperial2030.Engine
{
    public sealed class PlayerState
    {
        public PlayerState(Identity identity, int money)
        {
            Identity = identity;
            Money = money;
            Bonds = ImmutableList<Bond>.Empty;
        }

        private PlayerState(Identity identity, int money, ImmutableList<Bond> bonds)
        {
            Identity = identity;
            Money = money;
            Bonds = bonds;
        }

        public Identity Identity { get; }
        public int Money { get; }
        public ImmutableList<Bond> Bonds { get; }

        public string Name => Identity.Name;

        public IEnumerable<Bond> BondTotalPerNation => Bonds.GroupBy(
            bond => bond.Nation,
            (nation, bonds) =>
                bonds.Aggregate((b1, b2) => new Bond(b1.Nation, b1.Value + b2.Value, b1.Interest + b2.Interest)));

        [Pure]
        public Bond GetBondTotal(Identity nation) => Bonds
            .Where(bond => bond.Nation == nation)
            .Aggregate(
                new Bond(nation, 0, 0),
                (b1, b2) => new Bond(b1.Nation, b1.Value + b2.Value, b1.Interest + b2.Interest));

        [Pure]
        public IEnumerable<Bond> GetBonds(Identity nation) => Bonds
            .Where(bond => bond.Nation == nation);

        [Pure]
        public Maybe<PlayerState> WithMoney(int newMoney)
        {
            if (newMoney < 0)
                return default;
            
            return new PlayerState(Identity, newMoney, Bonds);
        }

        [Pure]
        public Maybe<PlayerState> AcquireBond(Bond newBond)
        {
            var newBonds = Bonds.Add(newBond);
            var newMoney = Money - newBond.Value;
            if (newMoney < 0)
                return default;
            
            return new PlayerState(Identity, newMoney, newBonds);
        }

        [Pure]
        public Maybe<PlayerState> AcquireBond(Bond newBond, Bond replacedBond)
        {
            if (newBond.Value <= replacedBond.Value)
                return default;

            var newMoney = Money - newBond.Value + replacedBond.Value;
            if (newMoney < 0)
                return default;

            return Bonds.TryReplace(replacedBond, newBond)
                .Select(newBonds => new PlayerState(Identity, newMoney, newBonds));
        }
    }
}
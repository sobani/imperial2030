﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Imperial2030.Engine.Utils
{
    public readonly struct Maybe<T>
    {
        /// <summary>
        /// Equivalent to `default(Maybe{T})`
        /// </summary>
        public static readonly Maybe<T> Nothing = new Maybe<T>();
        
        private readonly T _value;
        
        public Maybe(T value)
        {
            _value = value;
            HasValue = true;
        }
        
        public bool HasValue { get; }

        [return: MaybeNull]
        public T GetValueOrDefault() => _value;

        public T GetValueOrThrow(string message)
        {
            if (HasValue)
                return _value;
            
            throw new InvalidOperationException(message);
        }

        public bool TryGetValue(
            [MaybeNullWhen(false)] out T value)
        {
            value = _value;
            return HasValue;
        } 

        public T OrElse(T alternative)
        {
            if (HasValue)
                return _value;
            return alternative;
        }

        public Maybe<T> OrElse(Maybe<T> alternative)
        {
            if (HasValue)
                return this;
            return alternative;
        }

        public void Do(Action<T> action)
        {
            if (HasValue)
                action(_value);
        }

        public Maybe<TResult> Cast<TResult>()
        {
            if (HasValue && _value is TResult result)
                return result;

            return default;
        }

        public Maybe<TResult> Select<TResult>(Func<T, TResult> selector)
        {
            if (HasValue)
                return new Maybe<TResult>(selector(_value));
            
            return default;
        }

        public Maybe<TResult> Select<TResult>(Func<T, Maybe<TResult>> selector)
        {
            if (HasValue)
                return selector(_value);
            
            return default;
        }

        public Maybe<T> Where(Func<T, bool> predicate)
        {
            if (HasValue && predicate(_value))
                return this;

            return default;
        }

        public Maybe<TResult> Join<T2, TResult>(Maybe<T2> maybe2, Func<T, T2, TResult> join)
        {
            if (HasValue && maybe2.HasValue)
                return new Maybe<TResult>(join(_value, maybe2._value));
            
            return default;
        }

        public Maybe<TResult> Join<T2, TResult>(Maybe<T2> maybe2, Func<T, T2, Maybe<TResult>> join)
        {
            if (HasValue && maybe2.HasValue)
                return join(_value, maybe2._value);
            
            return default;
        }

        public IEnumerable<T> AsEnumerable()
        {
            if (HasValue)
                yield return _value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (HasValue)
                yield return _value;
        }

        public static implicit operator Maybe<T>(T value)
        {
            return new Maybe<T>(value);
        }
    }
}
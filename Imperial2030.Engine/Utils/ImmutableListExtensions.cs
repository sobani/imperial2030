﻿using System;
using System.Collections.Immutable;

namespace Imperial2030.Engine.Utils
{
    public static class ImmutableListExtensions
    {
        public static Maybe<ImmutableList<T>> TryReplace<T>(
            this ImmutableList<T> list,
            T oldValue,
            T newValue)
        {
            if (Equals(newValue, oldValue))
                return default;

            var index = list.IndexOf(oldValue);
            if (index == -1)
                return default;

            return list.SetItem(index, newValue);
        }

        public static Maybe<ImmutableList<T>> TryReplace<T>(
            this ImmutableList<T> list,
            T oldValue,
            Maybe<T> newValue)
        {
            return newValue.Select(nv => TryReplace(list, oldValue, nv));
        }

        public static Maybe<ImmutableList<T>> TryReplace<T, TKey>(
            this ImmutableList<T> list,
            T newValue,
            Func<T, TKey> keySelector)
        {
            var keyToFind = keySelector(newValue);
            var index = list.FindIndex(item => Equals(keySelector(item), keyToFind));
            if (index == -1)
                return default;

            var oldValue = list[index];
            if (Equals(oldValue, newValue))
                return default;

            return list.SetItem(index, newValue);
        }

        public static Maybe<ImmutableList<T>> TryReplace<T, TKey>(
            this ImmutableList<T> list,
            Maybe<T> newValue,
            Func<T, TKey> keySelector)
        {
            return newValue.Select(nv => TryReplace(list, nv, keySelector));
        }

        public static Maybe<ImmutableList<T>> TryUpdate<T>(
            this ImmutableList<T> list,
            Predicate<T> predicate,
            Func<T, T> update)
        {
            var index = list.FindIndex(predicate);
            if (index == -1)
                return default;

            var oldValue = list[index];
            var newValue = update(oldValue);
            if (Equals(oldValue, newValue))
                return default;

            return list.SetItem(index, newValue);
        }

        public static Maybe<ImmutableList<T>> TryUpdate<T>(
            this ImmutableList<T> list,
            Predicate<T> predicate,
            Func<T, Maybe<T>> update)
        {
            var index = list.FindIndex(predicate);
            if (index == -1)
                return default;

            var oldValue = list[index];
            var result = update(oldValue);

            return result.Select(
                newValue => Equals(oldValue, newValue)
                    ? Maybe<ImmutableList<T>>.Nothing
                    : list.SetItem(index, newValue));
        }

        public static Maybe<ImmutableList<T>> TryUpdate<T>(
            this Maybe<ImmutableList<T>> list,
            Predicate<T> predicate,
            Func<T, Maybe<T>> update)
        {
            return list.Select(l => TryUpdate(l, predicate, update));
        }

        public static Maybe<ImmutableList<T>> TryRemove<T>(
            this ImmutableList<T> list,
            T item)
        {
            var newList = list.Remove(item);
            if (newList == list)
                return default;
            return newList;
        }
    }
}
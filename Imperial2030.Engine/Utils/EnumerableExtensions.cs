﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Imperial2030.Engine.Utils
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Values<T>(
            this IEnumerable<Maybe<T>> source)
        {
            foreach (var maybe in source)
            {
                if (maybe.TryGetValue(out var value))
                    yield return value;
            }
        }

        public static IEnumerable<TResult> SelectMany<T1, T2, TResult>(
            this IEnumerable<T1> source,
            [InstantHandle] Func<T1, Maybe<T2>> maybeSelector,
            [InstantHandle] Func<T1, T2, TResult> resultSelector)
        {
            foreach (var item in source)
            {
                var maybe = maybeSelector(item);
                if (maybe.TryGetValue(out var maybeValue))
                    yield return resultSelector(item, maybeValue);
            }
        }

        public static IEnumerable<TResult> SelectMany<T1, T2, TResult>(
            this IEnumerable<T1> source,
            [InstantHandle] Func<T1, Maybe<T2>> maybeSelector,
            [InstantHandle] Func<T1, T2, Maybe<TResult>> resultSelector)
        {
            foreach (var item in source)
            {
                var maybe = maybeSelector(item);
                if (maybe.TryGetValue(out var maybeValue))
                {
                    var result = resultSelector(item, maybeValue);
                    if (result.TryGetValue(out var resultValue))
                        yield return resultValue;
                }
            }
        }
    }
}
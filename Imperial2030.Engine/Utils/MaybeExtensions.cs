﻿using System;
using JetBrains.Annotations;

namespace Imperial2030.Engine.Utils
{
    public static class MaybeExtensions
    {
        public static Maybe<TResult> Select<T1, T2, TResult>(this Maybe<(T1, T2)> maybe, Func<T1, T2, TResult> selector)
        {
            return maybe.Select(tuple => selector(tuple.Item1, tuple.Item2));
        }
        
        public static Maybe<TResult> Select<T1, T2, TResult>(this Maybe<(T1, T2)> maybe, Func<T1, T2, Maybe<TResult>> selector)
        {
            return maybe.Select(tuple => selector(tuple.Item1, tuple.Item2));
        }

        public static Maybe<TResult> Join<T1, T2, TResult>(this (Maybe<T1>, Maybe<T2>) tuple, Func<T1, T2, TResult> selector)
        {
            return tuple.Item1.Join(tuple.Item2, selector);
        }
        
        public static Maybe<TResult> Join<T1, T2, TResult>(this (Maybe<T1>, Maybe<T2>) tuple, Func<T1, T2, Maybe<TResult>> selector)
        {
            return tuple.Item1.Join(tuple.Item2, selector);
        }

        public static Maybe<TResult> SelectMany<T1, T2, TResult>(
            this Maybe<T1> source,
            Func<T1, Maybe<T2>> maybeSelector,
            Func<T1, T2, TResult> resultSelector)
        {
            return source.Select(t1 => maybeSelector(t1).Select(t2 => resultSelector(t1, t2)));
        }

        public static Maybe<TResult> SelectMany<T1, T2, TResult>(
            this Maybe<T1> source,
            [InstantHandle] Func<T1, Maybe<T2>> maybeSelector,
            [InstantHandle] Func<T1, T2, Maybe<TResult>> resultSelector)
        {
            return source.Select(t1 => maybeSelector(t1).Select(t2 => resultSelector(t1, t2)));
        }
    }
}
﻿using System.Collections.Immutable;

namespace Imperial2030.Engine.Utils
{
    public static class ImmutableDictionaryExtensions
    {
        public static Maybe<TValue> TryGet<TKey, TValue>(
            this ImmutableDictionary<TKey, TValue> dictionary,
            TKey key)
            where TKey: notnull
        {
            if (dictionary.TryGetValue(key, out var value))
                return new Maybe<TValue>(value);
            return default;
        }
    }
}
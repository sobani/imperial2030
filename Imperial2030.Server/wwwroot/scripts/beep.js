﻿
window.beep = (function () {
    let ctxClass = window.audioContext || window.AudioContext || window.webkitAudioContext
    let ctx = new ctxClass();
    return function () {
        let osc = ctx.createOscillator();

        osc.type = "sine";
        osc.frequency.value = 880;
        
        let duration = 70;

        osc.connect(ctx.destination);
        if (osc.noteOn) osc.noteOn(0);
        if (osc.start) osc.start();

        setTimeout(function () {
            if (osc.noteOff) osc.noteOff(0);
            if (osc.stop) osc.stop();
        }, duration);
    };
})();
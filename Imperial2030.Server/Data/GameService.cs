using System.Collections.Generic;
using System.Threading.Tasks;
using Imperial2030.Engine;
using Imperial2030.Engine.Choice;
using Imperial2030.Engine.Configuration;
using Microsoft.AspNetCore.SignalR;

namespace Imperial2030.Server.Data
{
    public class GameService
    {
        private readonly IHubContext<GameHub> _hub;
        private IChoice? _currentChoice;

        public GameService(IHubContext<GameHub> hub)
        {
            _hub = hub;
        }

        public async Task<IChoice> CreateNewGameAsync(IEnumerable<Identity> playerIdentities)
        {
            var gameStart = new InvestingGameStart();
            _currentChoice = gameStart.CreateGame(playerIdentities);
            
            await _hub.Clients.All.SendAsync("GameStateChanged");
            
            return _currentChoice;
        }

        public Task<IChoice?> GetGameAsync()
        {
            return Task.FromResult(_currentChoice);
        }

        public async Task SetCurrentChoiceAsync(IChoice newChoice)
        {
            _currentChoice = newChoice;
            
            await _hub.Clients.All.SendAsync("GameStateChanged");
        }
    }
}
﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace Imperial2030.Server.Data
{
    public class GameHub : Hub
    {
        public async Task SendGameStateChanged()
        {
            await Clients.All.SendAsync("GameStateChanged");
        }
    }
}